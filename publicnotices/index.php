<?
include_once("lib/arc2/ARC2.php");

/* configuration */ 
$config = array(
  /* remote endpoint */
  'remote_store_endpoint' => 'http://opendata.tellmescotland.gov.uk/sparql',
);

/* instantiation */
$store = ARC2::getRemoteStore($config);

$q = "SELECT DISTINCT * WHERE {
  ?s ?p ?o
}
LIMIT 10";
$q2 = "SELECT ?s ?g
WHERE { 
?s rdf:type ispin:Notice . 
?s georss:point ?g .
}";
$rows = $store->query($q2, 'rows');

var_dump($rows);

?>