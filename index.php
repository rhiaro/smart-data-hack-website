<? include 'top.php'; ?>

        <header class="light clearfix">
            <div class="color3-bg">
                <p class="wrapper unpad"><img src="img/edlogo_sm.jpg" width="70px" alt="The University of Edinburgh" /> Innovative Learning Week in Informatics <span class="right" style="margin-top:1.4em"><strong>18th - 22nd February 2013</strong></span></p>
            </div>
            <div class="color1-texture align-center clearfix"><div class="wrapper">
                <hgroup class="inner">
                    <h1 class="unpad">Smart Data Hack</h1>
                    <p><strong>Doing smart things with data to benefit local people</strong></p>
                </hgroup>
                <div class="w1of2"><div class="inner">
                    <p class="btn color1"><a href="hack.php" class="darker-border light-bg">Student?</a></p>
                    <p>If you're a University of Edinburgh student who wants to take part in the hack (which involves making something cool, learning new skills, eating free food and maybe winning great prizes), find out more here.</p>
                </div></div>
                <div class="w1of2"><div class="inner">
                    <p class="btn color1"><a href="workwithus.php" class="darker-border light-bg">Not a student?</a></p>
                    <p>There are loads of ways to support and benefit from this event, so we'd love to hear from you.  Maybe you have some data you want to see used in creative ways, a problem that needs solving, or expertise to share?</p>
                </div></div>
            </div></div>
        </header>

<? include 'end.php'; ?>
