<?php 
/*
UserCake Version: 2.0.1
http://usercake.com
*/
require_once("u/models/config.php");
securePage($_SERVER['PHP_SELF']);

$teams = getAllTeams();
$users = getNotInTeam();
if(empty($teams)) $teams = array();
if(empty($users)) $users = array();

include 'top.php';
?>

<? include 'top_hack.php'; ?>
<? include 'nav_hack.php'; ?>

<div class="wrapper lighter-bg inner clearfix">
	<h2 class="first">Teams &amp; Projects</h2>
	<p>Teams and projects will be listed here when the event gets underway.</p>
	<div class="w1of2"><div class="inner">
		<? 
		foreach($teams as $team){
			$aTeam = getTeamMembers($team['id']);
			echo "<h3>$team['name']}</h3>";
			if(!empty($aTeam)){
				foreach($aTeam as $person){
					echo "<p>";
					if($person['leader']) echo "<strong>";
					echo $person['displayname'];
					if($person['leader']) echo "</strong>";
					if(strlen($person['about']) > 0) echo " - <em>\"{$person['about']}\"</em>";
					echo "</p>";
				}
			}else{
				echo "<p><em>Nobody in this team</em></p>";
			}
		}
		?>
	</div></div>
	<div class="w1of2"><div class="inner">
		<h3>Registrants not yet in teams</h3>
		<?
		foreach($users as $user){
			echo "<p>{$user['display_name']}";
			if(strlen($user['about']) > 0) echo " - <em>\"{$user['about']}\"</em>";
			echo "</p>";
		}
		?>
	</div></div>
</div>
<? include 'end.php'; ?>
