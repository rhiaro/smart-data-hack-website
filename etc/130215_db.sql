-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: ilwhack13
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `reg_configuration`
--

DROP TABLE IF EXISTS `reg_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reg_configuration`
--

LOCK TABLES `reg_configuration` WRITE;
/*!40000 ALTER TABLE `reg_configuration` DISABLE KEYS */;
INSERT INTO `reg_configuration` VALUES (1,'website_name','Smart Data Hack in Innovative Learning Week'),(2,'website_url','http://data.inf.ed.ac.uk/hack/u/'),(3,'email','amy.guy@ed.ac.uk'),(4,'activation','0'),(5,'resend_activation_threshold','0'),(6,'language','models/languages/en.php'),(7,'template','css/main.css');
/*!40000 ALTER TABLE `reg_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reg_pages`
--

DROP TABLE IF EXISTS `reg_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(150) NOT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reg_pages`
--

LOCK TABLES `reg_pages` WRITE;
/*!40000 ALTER TABLE `reg_pages` DISABLE KEYS */;
INSERT INTO `reg_pages` VALUES (1,'account.php',1),(2,'activate-account.php',0),(3,'admin_configuration.php',1),(4,'admin_page.php',1),(5,'admin_pages.php',1),(6,'admin_permission.php',1),(7,'admin_permissions.php',1),(8,'admin_user.php',1),(9,'admin_users.php',1),(10,'forgot-password.php',0),(11,'index.php',1),(13,'login.php',0),(14,'logout.php',1),(15,'register.php',0),(16,'resend-activation.php',0),(18,'user_links.php',0);
/*!40000 ALTER TABLE `reg_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reg_permission_page_matches`
--

DROP TABLE IF EXISTS `reg_permission_page_matches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_permission_page_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reg_permission_page_matches`
--

LOCK TABLES `reg_permission_page_matches` WRITE;
/*!40000 ALTER TABLE `reg_permission_page_matches` DISABLE KEYS */;
INSERT INTO `reg_permission_page_matches` VALUES (1,1,1),(2,1,14),(4,2,1),(5,2,3),(6,2,4),(7,2,5),(8,2,6),(9,2,7),(10,2,8),(11,2,9),(12,2,14),(14,1,11),(15,2,11),(16,2,15);
/*!40000 ALTER TABLE `reg_permission_page_matches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reg_permissions`
--

DROP TABLE IF EXISTS `reg_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reg_permissions`
--

LOCK TABLES `reg_permissions` WRITE;
/*!40000 ALTER TABLE `reg_permissions` DISABLE KEYS */;
INSERT INTO `reg_permissions` VALUES (1,'New Member'),(2,'Administrator');
/*!40000 ALTER TABLE `reg_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reg_team_projects`
--

DROP TABLE IF EXISTS `reg_team_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_team_projects` (
  `team_id` int(8) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(140) COLLATE utf8_unicode_ci NOT NULL,
  `project_name` varchar(140) COLLATE utf8_unicode_ci NOT NULL,
  `project_pitch` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `repo` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(8) NOT NULL,
  `is_del` int(1) NOT NULL,
  `last_modified` int(16) NOT NULL,
  `last_modified_by` int(8) NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reg_team_projects`
--

LOCK TABLES `reg_team_projects` WRITE;
/*!40000 ALTER TABLE `reg_team_projects` DISABLE KEYS */;
INSERT INTO `reg_team_projects` VALUES (1,'Test','','','','',1,1,0,0),(2,'Test','','','','',1,1,0,0),(3,'Test','','','','',1,1,0,0),(4,'The best team ever','Test','asdfasdf','asdf','git',1,1,1358787950,1),(5,'Xtreme Sharks','Something cool','A cool project','','',4,1,1358788066,1),(6,'test','','','','',1,1,1359409696,1),(7,'Lithuanian Men of Digital Pizza','','','','',10,0,0,0),(8,'áƒš(à² ç›Šà² )áƒš','','','','',16,1,1360673869,16),(9,'Name Pending','','','','',19,0,0,0),(10,'go4itprincess','pretty cool ha?','\"It\'s not a bug; it\'s an undocumented feature!\"','http://go4itprincess.modulo.zone.ee','https://go4itprincess.googlecode.com/svn/trunk/',22,0,1360112795,22),(11,'Batkan','','','','',23,0,0,0),(12,'Team Tusive','Maria temporis II: the Bucket of Bits','','','',28,0,1360166142,27),(13,'Team Adverse Weather','','','','',18,1,1360190254,18),(14,'Team Adverse Weather','','','','',32,0,0,0),(15,'reallycooldudes','','','','',7,0,1360606794,7),(16,'A-Team','[insert really cool project name here]','Probably something web-based, kupos.','','',42,0,1360622820,42),(17,'Will Code for Food','','','','',46,0,0,0),(18,'87','','','','',53,0,0,0),(19,'Unnamed Team','','','','',31,0,0,0),(20,'MastersofHaskell','','','','',61,0,0,0),(21,'TequilaCartel','NoNameYet','No idea yet','','',63,0,1360772127,63),(22,'French Toast Mafia','','','','',66,0,0,0),(24,'chlpate_rezne','Tarantula','','http://www.lemonparty.tv/','',70,0,1360795006,65),(26,'Semantics Wizards','','','','',74,0,0,0),(27,'INF-YT','','','','',81,0,0,0),(28,'Test','','','','',1,1,1360940980,1);
/*!40000 ALTER TABLE `reg_team_projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reg_user_permission_matches`
--

DROP TABLE IF EXISTS `reg_user_permission_matches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_user_permission_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reg_user_permission_matches`
--

LOCK TABLES `reg_user_permission_matches` WRITE;
/*!40000 ALTER TABLE `reg_user_permission_matches` DISABLE KEYS */;
INSERT INTO `reg_user_permission_matches` VALUES (1,1,2),(2,1,1),(6,5,1),(7,6,1),(8,7,1),(9,8,1),(10,9,1),(11,10,1),(12,11,1),(13,12,1),(14,13,1),(15,14,1),(16,15,1),(17,16,1),(18,17,1),(19,18,1),(20,19,1),(21,20,1),(22,21,1),(23,22,1),(24,23,1),(25,24,1),(26,25,1),(27,26,1),(28,27,1),(29,28,1),(30,29,1),(31,30,1),(32,31,1),(33,32,1),(34,33,1),(35,34,1),(36,35,1),(37,36,1),(38,37,1),(39,38,1),(40,39,1),(41,40,1),(42,41,1),(43,42,1),(44,43,1),(45,44,1),(46,45,1),(47,46,1),(48,47,1),(49,48,1),(50,49,1),(51,50,1),(52,51,1),(53,52,1),(54,53,1),(55,54,1),(56,55,1),(57,56,1),(58,57,1),(59,58,1),(60,59,1),(61,60,1),(62,61,1),(63,62,1),(64,63,1),(65,64,1),(66,65,1),(67,66,1),(68,67,1),(69,68,1),(70,69,1),(71,70,1),(72,71,1),(73,72,1),(74,73,1),(75,74,1),(76,75,1),(77,76,1),(78,77,1),(79,78,1),(80,79,1),(81,80,1),(82,81,1),(83,82,1),(84,83,1),(85,84,1);
/*!40000 ALTER TABLE `reg_user_permission_matches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reg_users`
--

DROP TABLE IF EXISTS `reg_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `password` varchar(225) NOT NULL,
  `email` varchar(150) NOT NULL,
  `activation_token` varchar(225) NOT NULL,
  `last_activation_request` int(11) NOT NULL,
  `lost_password_request` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `title` varchar(150) NOT NULL,
  `sign_up_stamp` int(11) NOT NULL,
  `last_sign_in_stamp` int(11) NOT NULL,
  `matric_no` varchar(8) NOT NULL,
  `dietary` varchar(140) NOT NULL,
  `about` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reg_users`
--

LOCK TABLES `reg_users` WRITE;
/*!40000 ALTER TABLE `reg_users` DISABLE KEYS */;
INSERT INTO `reg_users` VALUES (1,'rhiaro','Amy Guy','7875296d58e39f1f8090a4b9a6118aa24da8548e25d0364ae4c33f8f42b2bff21','amy.guy@ed.ac.uk','c19b27f7ba82409de9a1a46e16abfd82',1358544794,0,1,'Ringleader',1358544794,1360940955,'1158216','Vegetarian','PhD student in CISA... you can contact me if you have any questions about the hack.'),(6,'Alex','Alexandra Kearney','4fb063d43da98cbb5aee22b17dbcdf6c3b569822c7ba778ce19381cccf239bc5b','kearney@ualberta.ca','10034ff885ff5c284611afebbf60340a',1359533124,0,1,'New Member',1359533124,0,'1210313','',''),(7,'bvborisov','Bilyan Borisov','5965de30e8ed4dfe84bac55f104ad0f8cc041eca57f01a50802560d836c348e91','s1101380@sms.ed.ac.uk','58dd8dfa9a4a7276477e19332aad8f76',1359661993,0,1,'New Member',1359661993,1360873421,'1101380','','I\'m taking part because I want to do something interesting during ILW. I\'m good with Java(and fairly well with any other language that\'s studied here as a part of some course) and I posses enough knowledge to enable me to learn on the spot. I believe a person\'s ability to code well is developed while doing some actual project so this would be a nice opportunity  to do so.'),(8,'goshe','Georgi Koshov','bcad76c14ebed77e78a8cfbbfb303f76846222c71d7118c992b92ac5a79b49a4d','g.koshov@sms.ed.ac.uk','b943c23af2a6fc29be25645f3c7c0f5f',1359725610,0,1,'New Member',1359725610,1360146507,'1034812','','I\'m a third year AI and CS undergrad with experience in developing web-apps using HTML5 and JavaScript and currently working with Python.\r\nI would be really happy if I get the chance to get my hands on and  learn more about MVC JS frameworks such as backbone.js, angular.js, etc. during the hack-week!'),(9,'bilgutei','Bilgutei Erdenebayar','60b20121c48a7b78798f6651f44a95142cc6cb9ed83e3b4f46cd991e4068b6c04','s1037071@inf.ed.ac.uk','c41e69c5b38593ab5e4c3e6616a0bd4b',1359726811,0,1,'New Member',1359726811,0,'1037071','','This event sounds very exciting so I wanted to try it out and get some experience, also learn more things. I have little experience in web development and programming.'),(10,'egzius','Egidijus Skinkys','1246451a25e38a8e72b3c689152bb9e8aa0b35c9698c72aa8e4deb5d434e14cf9','s1133101@sms.ed.ac.uk','0e61fefe403e1751a1b8c025728242ce',1359743838,0,1,'New Member',1359743838,1359745924,'1133101','',''),(11,'MatthiasGu','Motiejus Gudenas','043c2e70f24a22b5ca12c62d359b22ae3dbc66fa9f004ba9c4d35890c7091bdc7','lordmotiejus@yahoo.com','8fb2ecf685a99e7969055efbdd59f794',1359744063,0,1,'New Member',1359744063,1359759451,'1039137','Meat only (with chesse :) )',''),(12,'crunch','Ingvaras Merkys','f8164815043e4b72c19dd5d8011c0a4d37905201d4a2391353efb9d71a4964cdd','ingvaras@gmail.com','5012915916511241c17d3ea4f85daa0f',1359746247,0,1,'New Member',1359746247,1359746258,'1137752','',''),(13,'s1212622','Xiang Chen','95cde3e89367f8f0e60fd6882ef2c4e4b0a01d027153f684f15ffcfbfbe44b881','s1212622@sms.ed.ac.uk','15f03a7ae847dd7a7c032c0a209ad606',1359752807,0,1,'New Member',1359752807,1359752818,'1212622','','As an MSc in computer science, I am deeply attracted to the field of BigData and MapReduce. That\'s why I choose this event. I want to learn something from this event such as how to analyse data on different platforms or in different environments. I prefer to design algorithm than programming. But Java, C are OK.'),(14,'Dirzys','Lukas Dirzys','fb42e674b49fabcd61f30af4ca638ab3b46e1a1e989d13c1d02e296c239b7836f','l.dirzys@yahoo.com','68ffc0696c4f81e53fd681db92df22f0',1359754541,0,1,'New Member',1359754541,1360758340,'1119520','-',''),(15,'marpetr','Marijonas Petrauskas','c60615b30dc7ca4571718316fa19307d38d1fbf4ef40f2ca48ec869eb9cce902a','marpetr@gmail.com','a863429aa1ac116b007d978361ba76db',1359754835,0,1,'New Member',1359754835,1360438103,'1109170','-',''),(16,'jhirniak','Jaroslaw Hirniak','1ddf38c1ef8ce6a80a92c31525fd98143ced99fb5abf51d2f0aa48af78a2b1373','s1143166@sms.ed.ac.uk','c08b077eae3e8fd2babd4b56c5525861',1359762095,0,1,'New Member',1359762095,1360673827,'1143166','',''),(17,'danrr','Dan Ristea','f7658c4810d34bbe9a145a1a9cedd6d00bdff780c0231842433c81281edc4a594','s1244166@sms.ed.ac.uk','f4696a74a550e55092870eda18c3591c',1359840328,0,1,'New Member',1359840328,1360532803,'1244166','',''),(18,'haydenhaddock','Hayden Ball','9bf607fae4e94dc7120aabf40fbe83887346cca52e087f89e2176a3c834ef5660','hayden@haydenball.me.uk','4c76fb2e87bec10b23dfc9a49855f82b',1360005601,0,1,'New Member',1360005601,1360189692,'1202640','nil',''),(19,'camerongray','Cameron Gray','8b03ed246a6c94c5bbad2b95a70d70e30576ed263a6fd194d73f200eb052d4ad5','s1230461@sms.ed.ac.uk','d9e626936716893086b3d34c54ef74a5',1360098950,0,1,'New Member',1360098950,1360098957,'1230461','','Sounds like something I would really enjoy.\r\n\r\nI have good experience in PHP, Javascript/jQuery as well as moderate Perl, Java and C#.'),(20,'CraigDem','Craig Peden','6764723d50ec9ca26a263533587a074258bf7be48821ed200bd54fc8c9b8c6670','craig.peden.mail@gmail.com','978ddc2c097dc7244eb5c7e88b755bf5',1360099886,0,1,'New Member',1360099886,1360099910,'1229492','',''),(21,'jeffbr13','Ben Jeffrey','19176e5ebe26d7602aaafe930b5dc526769d399c9fdeb2821e752ea7d9204054b','mail@benjeffrey.net','2094f6c3d7e54a392353cf18a4d8700b',1360101137,0,1,'New Member',1360101137,1360940615,'1205856','','I want to work with some real data, and brush up my coding skills, especially in pair-programming and TDD.\r\n\r\nI know:\r\n* Python\r\n* little bit of Java\r\n* Golang\r\n* teensy bit of Haskell'),(22,'modulo','Margus Lind','4aefa4a6ab0fe0cd8d4fc7e82564af21cb3096615a5382cda91d41941bc460b82','margus.lind@gmail.com','b5b65f2feb33041a82a979c8e2882ec2',1360108437,0,1,'New Member',1360108437,1360798881,'1106798','none','I am interested...'),(23,'adamof','Stefan Adamov','921bfec7433a6e88e62358618c53458f101272964177784c9f63e4b350bae5382','stefan.adamof@gmail.com','e16fb036b466ba2ed7f502578af9e68b',1360145730,0,1,'New Member',1360145730,1360753908,'0956094','','Kit is a stalker'),(24,'kubakaszyk','Jakub Kaszyk','93cc524e591dbda6d73000f3c93ce9965d0d417955add2610c00fa596bbfb8929','kubakaszyk@gmail.com','593d978d7d0b0c042de6db499f3bc9ca',1360158920,0,1,'New Member',1360158920,1360781438,'1108328','',''),(25,'sstm','Craig Wilkinson','b5875d2c9e6e054c66f83f2721d0233545036c98badf1f196c4663c2d0bdf32f0','sdh@craigwilkinson.co.uk','380578b36229351a504de8253c85e5c0',1360159699,0,1,'New Member',1360159699,0,'1126144','Fresh grapes brought to my person periodically by a throng of nice ladies.',''),(26,'jsurbaitis','Joris Urbaitis','334c706061b5e19a09446941c7e6079c4a1bca25ece2e2a8a7d8192f99a3bc40a','j.s.urbaitis@gmail.com','23fed782fec5b0f2d86a953977ea1341',1360165833,0,1,'New Member',1360165833,1360166019,'1104617','',''),(27,'Ygfijj','Jonas Olsson','ff87c81d32c8fc1e7265d6371c4aa2588a0890ef16c132d9cc175fdbd2e7a4da6','jonas.es.olsson@home.se','844d989c05002cb7d20d9085864e6cd3',1360165858,0,1,'New Member',1360165858,1360165877,'1141754','',''),(28,'hchasestevens','Chase Stevens','ee97661c40d4b570fd90afdc8c6b1223fa933b744eb637a8049976f5506a63cfb','chase@chasestevens.com','2ed5dcfa3c600cd125acc0df7a48fffe',1360165922,0,1,'New Member',1360165922,1360165929,'1107496','I\'m vegetarian, and disprefer mushrooms (although they won\'t kill me).',''),(29,'Drayshak','Sky Welch','a394e1e5e5a17dbacffd94620e06cb21775f84e5131a9917289c8f47ca1284384','skyler.welch91@gmail.com','7c820125a6ca4cd48af8438117850197',1360169153,0,1,'New Member',1360169153,1360169179,'0918503','Strict vegetarian diet',''),(30,'ssabev','Stefan Sabev','7877d633455a6a42df93ef1dacf57e70f410223a909da0c76bd7f842783743d72','s1024819@sms.ed.ac.uk','04d127999cf2b5a972138d6d4b362cea',1360172885,0,1,'New Member',1360172885,1360172911,'1024819','N/A','I am taking part, because I\'d like to do something cool with the data that will be available on the hack day. I\'ve got a solid grip on Python/Flask, I can do a lot of SQL and lately JS and visualisations.'),(31,'s0812694','Barney Jackson','051c94638af275bdf3d55742db9ed52b911f4b430290085d2007fcad84ba89ac0','s0812694@sms.ed.ac.uk','97e3f8c3cbdce08959541eb23a1f1d87',1360173462,0,1,'New Member',1360173462,1360758828,'0812694','','I like challenges, building things, and big data. Particular experience in Javascript, Ruby on Rails, Python.'),(32,'craigsnowden','Craig Snowden','9a09bc261d5a334546e1b3e937d375fa4def13416130bd70c88ff2004d80f1c2c','craig@craigsnowden.com','c4a26cb71dc9b2c463c7f9f1aa87ace4',1360190148,0,1,'New Member',1360190148,1360190160,'1217709','Lots of Haribo.',''),(33,'R2ZER0','Rikki Guy','093a3ff113cb3f3b73da738f84080c5a6143812fc2ff122004a3b58d04f17d1c3','R.Guy-2@sms.ed.ac.uk','474b0660094f9a4743313f58e2c418b2',1360192747,0,1,'New Member',1360192747,1360192772,'1215551','',''),(34,'YPetrov','Yordan','7c1cc857588033cba5c93174566785887e7e2c1b86faa800f71d866a4e397d38d','y.v.petrov@sms.ed.ac.uk','56504437120654d9485c20ecd2fa2b7c',1360248656,0,1,'New Member',1360248656,1360249321,'0939895','N/A','I want to win the prizes.'),(35,'edran','Nantas Nardelli','3d0a9140688d381614f232eca3e683e15f38374293e9df2e3115a1680c5f55beb','nantas.nardelli@gmail.com','9b99ddbbc0dddc606ec6292102f32959',1360275082,0,1,'New Member',1360275082,1360623735,'1215573','','I\'m a first year undergrad Informatics student with several (I\'d say too many) years of C and assembler languages - that I fear now more than ever - behind my shoulder.\r\nI\'m currently learning C++ and Python and I am particularly interested in AI development, cognition and behaviour, so anything related to those elements is fine.\r\nWell, everything is actually fine: the fun lies in learning!'),(36,'easyCZ','Milan Pavlik','9ea6645145a074d7a3c2293682a98057d3d9f02d39558a4deca664d262b3d6f7b','pavlik.mil@gmail.com','d38c754f8f54185a8f1f313b1b2b68cf',1360322143,0,1,'New Member',1360322143,1360322170,'1115104','none','I would like to build either a java application or try and build a game in HTML5/js.'),(37,'Thoob','Theo Scott','518381b6cd84bb246737ccc49e8b629e8735663b4184fb8e4e077c463d0d739ac','tmscott10@hotmail.co.uk','32fae4191e2c6643f2e00311e6483213',1360330408,0,1,'New Member',1360330408,1360657726,'1231174','Vegetarian',''),(38,'vikev','Lubomir Vikev','179e11458c19fe70b0c8f51d02277e4ad3773bce21a9ab1017babb1e02568dfa2','l.k.vikev@sms.ed.ac.uk','95a5038b83b8cdf18d73d2a896e55c90',1360331289,0,1,'New Member',1360331289,1360772123,'1117764','','I am currently in my second year of SE course. I have good knowledge of HTML5, CSS, PHP. I also know some Java and C++. I have strong interests in web site building but I would like to learn how to make Android apps as well. I\'m looking forward to the Innovating Learning Week.'),(39,'lucabarone','Luca Barone','4c924b2472d340ea59a79fff89aa2a7adebf92228f3f06832aa9a805a8d6dd42f','luca.brn@gmail.com','6eaa3038700a810ca28796eb147db9d0',1360363905,0,1,'New Member',1360363905,1360363922,'1042809','','I\'m a third year CS student.\r\nI know a good deal of Python, decent knowledge of HTML5, some basics of JavaScript.\r\nI\'m participating to do something cool during ILW.'),(40,'Ignotas','Ignotas Sulzenko','4de4dde03818aae3fa9ede9321529e3b49362f5ba0ce69c5259b6d78174155d7b','tasignotas@gmail.com','2a08d54a929f280bd710411b9535cf97',1360365441,0,1,'New Member',1360365441,1360365450,'1137931','','I\'m a second year CS undergraduate, hoping to escape my comfort zone of JAVA and C/C++ by learning some new languages or developing an android app. I am also looking forward to learning and applying machine learning/data mining techniques to analyze the given data.'),(41,'vanyapetkova','Vanya Petkova','3aaf013e75f8350d0ae9b067ecb71be0bae873588c79e44fa7e8eb73ca157c884','vanyapetkova93@abv.bg','d55b8c3d67268d62716b19fe6f332c7a',1360420429,0,1,'New Member',1360420429,1360420448,'1227604','I want to learn about Java programming beyond the material which we learn in the lectures.',''),(42,'ReidE96','Euan Reid','317afcdd90db60a77de730fa760bf135a02425ce5a70b1c09134fea233536ad49','reide96@gmail.com','0fbbcdb3038b4336778f26bfd9f88999',1360460446,0,1,'New Member',1360460446,1360622694,'0932205','','Sounds like a fun activity to do for the ILW - plus, free food.\r\n\r\nI\'m a 3rd year SE student, I\'ve a lot of experience with PHP (inc. ooPHP), some with Javascript, and a fair dose of HTML (less with 5). Have also done some stuff with Android for the SELP, and various bits of Java. A little Python, a dose of CSS, a touch of Google App Engine, a solid dose of C# and VB.NET, and a willingness to figure things out as needed.'),(43,'drt0','Dimitar Todorov','7524de8df4d0d8cc0d498a47ece3ca29921f2bac439cde8705e2e07c63ce3ab88','todorov.dimitar3@gmail.com','6456c9b74aed4547c115f74dcab2c816',1360516021,0,1,'New Member',1360516021,1360701692,'1232046','none',''),(44,'matuskof','Matus Falis','5336d3f0c6362104612b46b498a042a4c4daeae68baa47c6df36f2405b3ce8811','matfalis@gmail.com','299f4520b8281989808626816cd7c3b8',1360524781,0,1,'New Member',1360524781,1360927266,'1206296','','skills: About a year and a half of java programming.'),(45,'yhristov','Yordan Hristov','a0e87b29a4d3c1a2eb6b1bebe2643f668a4a8bb9308e776f9561a8078513e0a2b','jrdnhr@gmail.com','a37398090d9b22b72d87e72ba608e254',1360543928,0,1,'New Member',1360543928,1360677603,'1220800','None','I am taking part mainly because of the experience - both programming and team-working. I have some web developing knowledge - HTML, CSS, JavaScript, basic PHP, some C++ and Visual Basic and some Java. I want to learn some Android (or any other type of) mobile app development but all in all any kind of programming experience will be good.'),(46,'Bannnat','Martin Asenov','b3efb5c4fd04d171c7ab0db4f34888d739a3c09f905bbae3769d3c9720574999b','m.a.asenov@gmail.com','76f2b294af10a731fab8eae33fa1da7e',1360597150,0,1,'New Member',1360597150,1360717962,'1247380','','I want to get a feeling of what it is to work as a programmer 7 hours a day. Good knowledge of Java.'),(47,'AmyQ','Mengtian Qiu','ec9e739e991e9bdb52c5d224c3d14335e1dcfe808321cb026c9be15e694f2c6d9','qiumengtian@qq.com','7ce3b6c9c0e0d1ecc919e7aef3735934',1360604515,0,1,'New Member',1360604515,1360604532,'1238998','Chinese food','It seems to me that this is a great opportunity to apply theory and also learn new stuff that\'s not in the lecture.   I have never been exposed to web-programming, this is entirely new to me.'),(48,'seuthesIII','Angel Ignatov','1d9163edebea0512d65aa92c148fb35643ad23854cb07d84f4cff25d14933c84c','a.ignatov@sms.ed.ac.uk','e9637c213efa0c38fef1564db0b27b68',1360623603,0,1,'New Member',1360623603,1360626515,'1006192','n/a','I want to improve my team working skills.\r\nI know a little of Java, C++, some web design'),(49,'LBarker','Lewis Barker','38fed21d27da58f0e962fd9f0f8418d352bd07281561e02e00e7752687a11fce1','s1236818@inf.ed.ac.uk','a67354937e3e52930e9718928d95ab68',1360624525,0,1,'New Member',1360624525,1360624535,'1236818','',''),(50,'oliland','oliland','f7ab00dd3292c574a2577e733ada2899a282d6de50c769c2ae2dce50b0da5c596','oliver@kingshott.com','fd0f2a5b9cb91678042bf8e4d022d989',1360671664,0,1,'New Member',1360671664,1360671671,'0817277','kiss from KitB',''),(51,'alvindemon','Zechao Li','fd4202dd9cc3c9ea888b6b2f5b4b75c1c895d9dee94be5c01cf940fced32111a9','alvindemon@hotmail.com','cf833be2d04759346d77415216fb9e78',1360686684,0,1,'New Member',1360686684,1360707268,'1204053','I want to be in one team with Jinge Dai and Jie Su. Thank you.','I take part in because I want to learn some new skills which I am interest in. I am good at C#,C++,java and HTML.'),(52,'dimy93','Dimitar Dimitrov','d5edf13d76ef5060cdafd7b6bd9adc0ff6403725f1cda75aa32ac70450e43914b','dimy93@abv.bg','4bdfb6edffe86b0771e740e5b8e9dc09',1360695046,0,1,'New Member',1360695046,1360695128,'1207825','','I hope to gain experience in teamwork and write some interesting code. I have experience in writing in C/C++ and C#'),(53,'dmoney','Jinge Dai','8a8501da3b471ddecbcbb2441ef30f97246b1ff78506cfe9c3bfb0fe5f395bb04','dai.jinge@gmail.com','37b7de4fa0059ef402741f172447db63',1360699373,0,1,'New Member',1360699373,1360699387,'1238262','','I am looking for improving my programming experience and learn some skills of javascript. I am good at java and hand on python.'),(54,'wishf','Matthew Summers','6f70a1de236f45e25cd3811e3aed839e03845987774784062c510c3719b165436','matt@wishf.co.uk','09e67735a476083a67f3b2bf46fbcaec',1360701728,0,1,'New Member',1360701728,1360701736,'1232732','','Taking part to learn new skills (such as android app dev, data visualisation, web dev), and also for pizza and prizes. I\'ve got experience in C#, Java and Haskell, mainly C#, and would like to break out of that little comfort-zone niche bit thing.'),(55,'whale','Boris Penev','357c288766d5b74c6ab01a26b14e6e8eb700dea61410a30f044537085f92009fc','s1249355@inf.ed.ac.uk','1ab293bf2e36faea204f81e47a9b14be',1360708113,0,1,'New Member',1360708113,1360708120,'1249355','I don\'t like junk food, fats (pork, beef). I like fruits, pizza, juices, water, etc.',''),(56,'jack','JieSu','cfff484c8f6551f982de6a95a214b9f0a9d60224969e9d98ee17d88d1395cbe57','sujie1987@gmail.com','9adb0fda3aca2a80b0e9cdd1ee926349',1360710594,0,1,'New Member',1360710594,1360711775,'1210663','I do not know the event in detail now. So I have no special requirements for the present.','This sounds very existing. And I am interested in the event related to the data area'),(57,'dlukas','Lukas Danev','4041ad3faa6258db88665f0287139a27a233030f704e80d9c48f129eb60a90096','s1245738@sms.ed.ac.uk','76a11e798a4dadba03d6cea74410d1ec',1360715377,0,1,'New Member',1360715377,1360715391,'1245738','none','First year'),(58,'lamejorieva','Ieva','35df03ef385c624263dc633cc5a1fd2e90d853168db9858aaa951b766cc7d835a','lamejorieva@gmail.com','36820127aa24d64d75eeb8c68a2267a6',1360757894,0,1,'New Member',1360757894,1360757917,'1136804','None.','Taking part for fun. Want to learn some coding. Good presentation skills, some new ideas.'),(59,'chriswait91','Chris Wait','3dd7fa7f619c64ce23b76b1238a20164ed548402aaa939ab1c7661231ef058580','chriswait91@gmail.com','7fa5d56536f60724b405cf7b40b6cb8f',1360759008,0,1,'New Member',1360759008,1360759017,'0926370','',''),(60,'lynne','Lynne Wallace','35c8466ec15341011968b8582865b49de0c7f52c749eb184bcaac8fa2a0efe735','s0829961@sms.ed.ac.uk','1033997d754170103102f3f85fdbfe4f',1360765620,0,1,'New Member',1360765620,1360765632,'0829961','',''),(61,'masterofhaskell','Christiaan Swart','624bf21a44ce7a6757b8ef6bfb4ba4a4b24cd94f1d1eb33d048ac4663bdcf6a37','windupcat2012@gmail.com','8a3c564e68cad8ba73022708fb567ef5',1360770781,0,1,'New Member',1360770781,1360770795,'1210107','','I think it is a good chance to get to know how programming is done in real life.'),(62,'Ness','Nestor Hernandez','379576cbd8ea277e9a803f27a4e404e21940399fc3e0c7f95376909f0ba2cb572','nessjav@gmail.com','8ce6e9199b9ab1e5232dba93e9b9c94e',1360770945,0,1,'New Member',1360770945,1360770975,'1215836','','I have enthusiasm for technology projects which involve team work and putting ideas into the real world. \r\nI have and mixed background of electronics and computer science, and I\'m doing a master in acoustics in music at this time, I like to think in ideas that can mix all my interest into one project, coming to this event will help me to improve my programming abilities and have fun with one week project'),(63,'ppdany','Jose Daniel Leal Avila','9faffc80ecfd695710146c3e146a161761188eb19f1d26be9091bfc364b4a2f27','ppdany@gmail.com','127461e05d5fe357ccda01224fa1d400',1360771047,0,1,'New Member',1360771047,1360771059,'1217971','','Background in electronics, doing an MSc by Res. in Interdisciplinary Creative Practices. \r\nInterested in learning all kinds of stuff!! Particularly for this, web stuff and mobile apps. Not much experience in web programming, just very little of php, javascript and SQL... but plenty in C, embedded C, Java and embedded Linux.\r\nLet\'s do something world changing!'),(64,'aivanis','Andrej Ivanis','1cfd44a46195b771093511811926e98f0b11fe28356aeb7f53b1d6c7136fb80f3','anivanis@gmail.com','aa626e338a63167596ea47c2fd354dc5',1360783838,0,1,'New Member',1360783838,1360873425,'1210443','','I\'m would like to improve my knowledge of web design.\r\nI have 10 years of programming experience, and I\'ve been making App Store iOS applications (mostly games and entertainment) for almost 5 years.\r\nFor last few years I\'ve been working mostly with Unity 3D engine, 3d modeling, graphics, sound effects and C#. Older project include C, C++, Web Sites, MS SQL, Objective C, C# for web servers, Java, JavaScript, Flash, ActionScriptâ€¦\r\nI would like to make mobile app, website or something else.'),(65,'rastarobbie','Robert Lacok','ad12045aebd0f3416e510879ed1650ab2abee78c1e40305a0f99512624a17f4d1','robo.lacok@gmail.com','177728f14453ce29d277d3f9a0dd329c',1360783873,0,1,'New Member',1360783873,1360935299,'1206494','',''),(66,'finsqm','Finlay Scott Qazi McAfee','08c3e0907997666dde36439d25b98b747c04d20d2ed0c49f7fca2464117d07302','finsqm@gmail.com','5a328ea0c0d47fa7801fea5a2a795895',1360785526,0,1,'New Member',1360785526,1360937427,'1220880','Allergy to Nuts',''),(67,'shoeyn','Nathan Shoemark','e13e930380697d4acf9030d81a59272db75a83c11fa2b02a8986e278cc866bf2c','n.shoemark@gmail.com','3db3027860801dc1a9d0df835a4c046a',1360785550,0,1,'New Member',1360785550,1360785573,'1239853','','to learn'),(68,'dc27','David Currie','7964d060215943762929310151608aa8ccb6841a12554f03abd14ef48bc49fcae','dcurrie27@gmail.com','095bc469b75e65e8db45ab69dda17038',1360786148,0,1,'New Member',1360786148,1360786163,'1122002','',''),(69,'enthusiast94','Manas Bajaj','60f753dc95cad9582ab425071e8530684fcb56c73ec351c8a923421cbf31c5b68','manas_bajaj007@hotmail.com','338dfd82a5ec74692d8a3cb549238162',1360789625,0,1,'New Member',1360789625,1360789631,'1265676','','Obviously NOT for the free food...I love programming : )'),(70,'matk0','Matej Mezes','cb6776008bf216b52b4fcb89f51f90c683f343f9489f86019b04134ddb8454946','matej.mezes@gmail.com','fba955738c22aa02e3999b4ef70b4507',1360794767,0,1,'New Member',1360794767,1360858001,'1204329','none','I see this as a brilliant opportunity to gain new set of skills as I am new to hacking. But with strong IT background I consider myself as a quick learner.'),(71,'andrikap','Andreas Kapourani','19c7acf109665ff02eb925153e888ffcc7a4b10c72985628242328e51f14332fe','s1263191@sms.ed.ac.uk','3962b952553b9ddb2b982dcaf3a443f7',1360795822,0,1,'New Member',1360795822,1360849426,'1263191','',''),(72,'XapaJIaMnu','Nikolay Bogoychev','c7a8adf5e3111010a6cfc9cb50a7f8242fe09090419d42fdea141b6c31271778c','nheart@gmail.com','4088eb1f335824a4f1b1ebf2b4a8bc09',1360842552,0,1,'New Member',1360842552,1360928438,'1031254','N/A','A third year AI/CS student looking to practice machine learning with real data.'),(73,'s1202144','Mattias Appelgren','5b4c1ac4592d995f5baef732481a5d94d086a25aa779abde9b200d50130e1bb49','mattiasappel@gmail.com','e8054b44002cd28f529a0579ef601644',1360843751,0,1,'New Member',1360843751,1360843765,'1202144','','First year inf student. I\'m joining in because I want to try some \"real\" programing. I have limited Haskell Java and Python abilities. (First two from class and the third self taught although I doubt I will remember many useful things there)'),(74,'rustamli','Turan Rustamli','0cc5c93f4e8be6ca6c49ffbdd30f80cc1fad170df3e0e74574ea1fa3c0d3ddc9b','s1263388@sms.ed.ac.uk','d5d249f655f1b91c08746adefabe41d2',1360849253,0,1,'New Member',1360849253,1360849329,'1263388','','Some highlights: Taking MASWS course, done IRR related to Semantic Web Search (Investigating things like Google\'s Knowledge Graph), Good programming skills: JavaScript, Python, Java; some UI design skills\r\n\r\nFind out more here: http://rustam.li/'),(75,'Ixtreon','Yordan Stoyanov','4158c82db5d2bea115204e879f08a009d157634b0cd3809da397fa7c93965c4c3','ixtreon@gmail.com','5b054abf68521a86e81a50b28834aced',1360849961,0,1,'New Member',1360849961,1360877505,'1141301','',''),(76,'BuckenBerry','Stanley Wang','e23dee60f71acac042b17591075d6e93decfc3ed88fc2bbd3dd5aa2586291792a','stanley.wang1995@gmail.com','bc783f2700de93b460dbb9728d5b9763',1360862834,0,1,'New Member',1360862834,1360862844,'1226594','None',''),(77,'turkeypi','luke mcauley','48a7375f1919073afe9fa9f8b6ee1cf3496fbb965d632dbb7975f4498c82ff4a5','luke.p.mcauley@gmail.com','4a8a9c39901d73662477cc986f6a6fec',1360867550,0,1,'New Member',1360867550,1360867563,'1217093','','Taking part for fun and to learn new skills. Would like to learn some phone or web app development.'),(78,'maalik','Malik Moosa','f840ca060aa03f85ae225f05870d9e5fb87ca52d41e6b16daf260d7374b18c791','s1245609@sms.ed.ac.uk','feb55c626b1bb833e6b0dcd4900d5ac0',1360871009,0,1,'New Member',1360871009,1360871019,'1245609','',''),(79,'msanatan','Marcus Sanatan','134315bdfc52086974c57220fafeb10164fb9a64e58251b556a8ebda1ad126fb3','msanatan@gmail.com','e3f1cdbe9dbceebe11f8192da6a9c524',1360871486,0,1,'New Member',1360871486,1360871499,'1039549','','3rd Year AI and CS student with fair knowledge of Python, Java, Haskell (of course), HTML and JavaScript. Despite lack of extensive use I am adept with SQL. \r\n\r\nThroughout my academic life I have individually been pretty successful, but as I\'m doing SDP (3rd year group project) it\'s clear that my best skill is with integrating ideas via pair programming. Aside from my technical ability I think my communicative skills set me apart.'),(80,'daliusgr','Dalius','0e2e6c86fdd41736a853fcd49da0a52c539eaf00c41ef0aa0a4ae6caa5af97c65','grazinskisd@gmail.com','dfb8d648565b4a36d0ea5d388c143eb0',1360878940,0,1,'New Member',1360878940,1360878956,'1247598','','As for a first year, my objective is to learn as much as i can and get experience in teamwork. Have basic knowledge of Java, C++ and android app dev. ;D'),(81,'JMac','Jamie MacDonald','86edb217247c2d2e79720e0628922488372ebf2f2d24fea4972b5ef61f24c6e59','s1219478@inf.ed.ac.uk','cd5c375fa1115e794f7f213f566c9de2',1360880653,0,1,'New Member',1360880653,1360880662,'1219478','Looking to join team \"Name Pending\"',''),(82,'lhparker','Lucy Parker','d781b1ea1a216aa2906ad0b03199b41a56ba6fce65d19d263c7ea360bf345d732','s1103154@sms.ed.ac.uk','57f65d3233640a92f6429ce88db45484',1360920004,0,1,'New Member',1360920004,1360920017,'1103154','Vegetarian',''),(83,'nklinkachev','Nikola Klinkachev','7563ef1f7f656ad86a1f81d5494df2a7d88c947984a96ed71b983b9bfbfe6fc08','nklinkachev@gmail.com','9e2c049b37f11962aefe8287e6a3d8fd',1360945338,0,1,'New Member',1360945338,1360945354,'1208988','',''),(84,'andriusa2','Andrius Å½iÅ«kas','c49ca0ee9160e03cf5caa2e4da6d89c0ef6823148efe71dc53765c7a39b94b398','andriusa2@gmail.com','3513c72195f1a94a11d3fe5924b8bc5d',1360947884,0,1,'New Member',1360947884,1360952471,'1208447','None','I have spent more than half of decade solving various algorithmic/data structure based tasks on plethora of imperative/objective languages (though I have a particular liking of c++ and ruby). I am somewhat interested in robust data analysis, hence the registration.');
/*!40000 ALTER TABLE `reg_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reg_users_teams`
--

DROP TABLE IF EXISTS `reg_users_teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_users_teams` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `user_id` int(8) NOT NULL,
  `team_id` int(8) NOT NULL,
  `leader` int(1) NOT NULL,
  `date_modified` int(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reg_users_teams`
--

LOCK TABLES `reg_users_teams` WRITE;
/*!40000 ALTER TABLE `reg_users_teams` DISABLE KEYS */;
INSERT INTO `reg_users_teams` VALUES (4,4,5,1,1358786622),(10,10,7,1,1359745928),(11,11,7,0,1359746028),(12,12,7,0,1359746273),(13,14,7,0,1359754563),(14,15,7,0,1359754858),(16,19,9,1,1360099668),(17,20,9,0,1360099920),(19,22,10,1,1360108973),(20,17,10,0,1360111827),(21,23,11,1,1360146132),(22,8,11,0,1360146527),(23,28,12,1,1360165939),(24,27,12,0,1360165972),(25,26,12,0,1360166025),(26,30,11,0,1360172955),(28,32,14,1,1360190209),(29,18,14,0,1360190267),(30,33,9,0,1360192812),(31,34,11,0,1360249467),(35,7,15,1,1360605432),(36,24,15,0,1360607640),(37,42,16,1,1360622752),(39,49,9,0,1360624638),(40,37,9,0,1360657746),(41,46,17,1,1360666066),(42,43,17,0,1360672290),(44,16,15,0,1360674471),(45,45,17,0,1360677615),(46,38,15,0,1360685505),(47,52,17,0,1360695136),(48,53,18,1,1360699611),(49,51,18,0,1360707275),(50,55,17,0,1360708150),(51,56,18,0,1360710634),(52,31,19,1,1360758863),(53,59,19,0,1360759022),(54,61,20,1,1360770839),(55,63,21,1,1360771375),(56,62,21,0,1360771420),(57,66,22,1,1360785697),(58,67,22,0,1360785711),(60,68,22,0,1360786366),(61,69,20,0,1360789649),(62,70,24,1,1360794886),(63,65,24,0,1360794906),(65,72,11,0,1360842624),(66,74,26,1,1360849542),(67,71,26,0,1360849562),(68,78,20,0,1360871054),(69,81,27,1,1360881008),(70,44,10,0,1360927297),(71,21,27,0,1360940658);
/*!40000 ALTER TABLE `reg_users_teams` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-02-15 19:23:37
