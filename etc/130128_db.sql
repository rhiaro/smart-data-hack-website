-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 28, 2013 at 03:53 PM
-- Server version: 5.0.95-community-cll-lve
-- PHP Version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ilwhack13`
--

-- --------------------------------------------------------

--
-- Table structure for table `reg_configuration`
--

CREATE TABLE IF NOT EXISTS `reg_configuration` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(150) NOT NULL,
  `value` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `reg_configuration`
--

INSERT INTO `reg_configuration` (`id`, `name`, `value`) VALUES
(1, 'website_name', 'Smart Data Hack in Innovative Learning Week'),
(2, 'website_url', 'http://rhiaro.co.uk/ilwhack'),
(3, 'email', 'amy.guy@ed.ac.uk'),
(4, 'activation', '0'),
(5, 'resend_activation_threshold', '0'),
(6, 'language', 'models/languages/en.php'),
(7, 'template', 'css/main.css');

-- --------------------------------------------------------

--
-- Table structure for table `reg_pages`
--

CREATE TABLE IF NOT EXISTS `reg_pages` (
  `id` int(11) NOT NULL auto_increment,
  `page` varchar(150) NOT NULL,
  `private` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `reg_pages`
--

INSERT INTO `reg_pages` (`id`, `page`, `private`) VALUES
(1, 'account.php', 1),
(2, 'activate-account.php', 0),
(3, 'admin_configuration.php', 1),
(4, 'admin_page.php', 1),
(5, 'admin_pages.php', 1),
(6, 'admin_permission.php', 1),
(7, 'admin_permissions.php', 1),
(8, 'admin_user.php', 1),
(9, 'admin_users.php', 1),
(10, 'forgot-password.php', 0),
(11, 'index.php', 1),
(13, 'login.php', 0),
(14, 'logout.php', 1),
(15, 'register.php', 1),
(16, 'resend-activation.php', 0),
(18, 'user_links.php', 0);

-- --------------------------------------------------------

--
-- Table structure for table `reg_permissions`
--

CREATE TABLE IF NOT EXISTS `reg_permissions` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `reg_permissions`
--

INSERT INTO `reg_permissions` (`id`, `name`) VALUES
(1, 'New Member'),
(2, 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `reg_permission_page_matches`
--

CREATE TABLE IF NOT EXISTS `reg_permission_page_matches` (
  `id` int(11) NOT NULL auto_increment,
  `permission_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `reg_permission_page_matches`
--

INSERT INTO `reg_permission_page_matches` (`id`, `permission_id`, `page_id`) VALUES
(1, 1, 1),
(2, 1, 14),
(4, 2, 1),
(5, 2, 3),
(6, 2, 4),
(7, 2, 5),
(8, 2, 6),
(9, 2, 7),
(10, 2, 8),
(11, 2, 9),
(12, 2, 14),
(14, 1, 11),
(15, 2, 11),
(16, 2, 15);

-- --------------------------------------------------------

--
-- Table structure for table `reg_team_projects`
--

CREATE TABLE IF NOT EXISTS `reg_team_projects` (
  `team_id` int(8) NOT NULL auto_increment,
  `team_name` varchar(140) collate utf8_unicode_ci NOT NULL,
  `project_name` varchar(140) collate utf8_unicode_ci NOT NULL,
  `project_pitch` varchar(1024) collate utf8_unicode_ci NOT NULL,
  `website` varchar(128) collate utf8_unicode_ci NOT NULL,
  `repo` varchar(128) collate utf8_unicode_ci NOT NULL,
  `created_by` int(8) NOT NULL,
  `is_del` int(1) NOT NULL,
  `last_modified` int(16) NOT NULL,
  `last_modified_by` int(8) NOT NULL,
  PRIMARY KEY  (`team_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `reg_team_projects`
--

INSERT INTO `reg_team_projects` (`team_id`, `team_name`, `project_name`, `project_pitch`, `website`, `repo`, `created_by`, `is_del`, `last_modified`, `last_modified_by`) VALUES
(1, 'Test', '', '', '', '', 1, 1, 0, 0),
(2, 'Test', '', '', '', '', 1, 1, 0, 0),
(3, 'Test', '', '', '', '', 1, 1, 0, 0),
(4, 'The best team ever', 'Test', 'asdfasdf', 'asdf', 'git', 1, 1, 1358787950, 1),
(5, 'Xtreme Sharks', 'Something cool', 'A cool project', '', '', 4, 1, 1358788066, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reg_users`
--

CREATE TABLE IF NOT EXISTS `reg_users` (
  `id` int(11) NOT NULL auto_increment,
  `user_name` varchar(50) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `password` varchar(225) NOT NULL,
  `email` varchar(150) NOT NULL,
  `activation_token` varchar(225) NOT NULL,
  `last_activation_request` int(11) NOT NULL,
  `lost_password_request` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `title` varchar(150) NOT NULL,
  `sign_up_stamp` int(11) NOT NULL,
  `last_sign_in_stamp` int(11) NOT NULL,
  `matric_no` varchar(8) NOT NULL,
  `dietary` varchar(140) NOT NULL,
  `about` varchar(512) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `reg_users`
--

INSERT INTO `reg_users` (`id`, `user_name`, `display_name`, `password`, `email`, `activation_token`, `last_activation_request`, `lost_password_request`, `active`, `title`, `sign_up_stamp`, `last_sign_in_stamp`, `matric_no`, `dietary`, `about`) VALUES
(1, 'rhiaro', 'Amy Guy', '7875296d58e39f1f8090a4b9a6118aa24da8548e25d0364ae4c33f8f42b2bff21', 'amy.guy@ed.ac.uk', 'c19b27f7ba82409de9a1a46e16abfd82', 1358544794, 0, 1, 'Ringleader', 1358544794, 1359117395, '1158216', 'Vegetarian', 'PhD student in CISA... you can contact me if you have any questions about the hack.');

-- --------------------------------------------------------

--
-- Table structure for table `reg_users_teams`
--

CREATE TABLE IF NOT EXISTS `reg_users_teams` (
  `id` int(8) NOT NULL auto_increment,
  `user_id` int(8) NOT NULL,
  `team_id` int(8) NOT NULL,
  `leader` int(1) NOT NULL,
  `date_modified` int(16) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `reg_users_teams`
--

INSERT INTO `reg_users_teams` (`id`, `user_id`, `team_id`, `leader`, `date_modified`) VALUES
(4, 4, 5, 1, 1358786622);

-- --------------------------------------------------------

--
-- Table structure for table `reg_user_permission_matches`
--

CREATE TABLE IF NOT EXISTS `reg_user_permission_matches` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `reg_user_permission_matches`
--

INSERT INTO `reg_user_permission_matches` (`id`, `user_id`, `permission_id`) VALUES
(1, 1, 2),
(2, 1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
