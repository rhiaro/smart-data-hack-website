        <footer class="wrapper"><div class="w1of1 clearfix">
            <div class="w2of3"><div class="inner">
                <h3>About</h3>
                <p>The Smart Data Hack is running as part of <a href="http://www.inf.ed.ac.uk/student-services/teaching-organisation/taught-course-information/informatics-innovative-learning-week-2013">Innovative Learning Week</a> in Informatics at the University of Edinburgh.</p>
                <p>We hope to encourage partnerships between local businesses, community organisations and student developers, and promote the use of Open Data for creative, socially empowering applications.</p>
                <p>The twitter hashtag for the event is <strong>#ilwhack</strong>.</p>
            </div></div>
            <div class="w1of3"><div class="inner">
                <h3>Contact</h3>
                <p>Ewan Klein <em>{ewan at staffmail.ed.ac.uk}</em></p>
                <p>Michael Rovatsos <em>{mrovatsos at inf.ed.ac.uk}</em>
                <p>Amy Guy <em>{amy.guy at ed.ac.uk}</em></p>
            </div></div>
            <div class="w1of3"><div class="inner">
            </div></div>
        </div></footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.3.min.js"><\/script>')</script>
        <script src="<?=$path?>js/main.js"></script>
        <script type="text/javascript">
          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-37786354-1']);
          _gaq.push(['_trackPageview']);
          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
        </script>
    </body>
</html>
