<?
require_once('lib/recaptchalib.php');
if(isset($_POST['sub'])){
	if(isset($errors)) unset($errors);
	if(isset($sent)) unset($sent);
	$privatekey = "6Lc4N9wSAAAAAH2GYofnp576vfySHhRGeADGhVUx";
	$resp = recaptcha_check_answer ($privatekey,
	                            $_SERVER["REMOTE_ADDR"],
	                            $_POST["recaptcha_challenge_field"],
	                            $_POST["recaptcha_response_field"]);

	if(!$resp->is_valid) $errors['cap'] = "The reCAPTCHA wasn't entered correctly ({$resp->error})";
	if(!isset($_POST['name']) || strlen($_POST['name']) < 1) $errors['name'] = "We need to know your name!";
	if(!isset($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) $errors['email'] = "Please enter your email address so that we can get back to you.";
	if(!isset($_POST['you']) || strlen($_POST['you']) < 20) $errors['you'] = "Please tell us a little bit more about why you're interested.";

	if(count($errors) == 0){
		// send email
		$msg = "Name: {$_POST['name']}\n";
		$msg .= "Email: {$_POST['email']}\n";
		$msg .= "Website: {$_POST['web']}\n";
		$msg .= "Interested in: ";
		if(isset($_POST['cha'])) $msg .= "{$_POST['cha']} ";
		if(isset($_POST['dat'])) $msg .= "{$_POST['dat']} ";
		if(isset($_POST['men'])) $msg .= "{$_POST['men']} ";
		if(isset($_POST['wor'])) $msg .= "{$_POST['wor']} ";
		if(isset($_POST['spo'])) $msg .= "{$_POST['spo']} ";
		if(isset($_POST['oth'])) $msg .= "{$_POST['oth']} ";
		$msg .= "\n";
		$msg .="More info: {$_POST['you']}\n";

		$to      = 'amy.guy@ed.ac.uk, ewan@staffmail.ed.ac.uk';
		$subject = 'Interest in ILW Hack';
		$headers = 'From: ' . $_POST['email'] . "\r\n" .
		    'Reply-To: ' . $_POST['email'] . "\r\n" .
    		'X-Mailer: PHP/' . phpversion();

		if(mail($to, $subject, $msg, $headers)){
			$sent = true;
			unset($_POST);
		}else{
			$sent = false;
			$errors['mail'] = "Something went wrong on the server, please try again.";
		}
	}else{
		$sent = false;
		//page renders again..
	}
}
?>
