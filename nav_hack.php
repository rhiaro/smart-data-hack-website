        <nav id="sticky-nav" class="clearfix">
            <ul class="wrapper inner">
                <li><a href="<?=$path?>hack.php#top">Home</a></li>
                <li><a href="<?=$path?>hack.php#sch">Schedule</a></li>
                <li><a href="<?=$path?>hack.php#spo">Sponsors &amp; Partners</a></li>
                <li><a href="<?=$path?>hack.php#men">Workshops &amp; Mentors</a></li>
                <li><a href="<?=$path?>hack.php#cha">Challenges &amp; Prizes</a></li>
                <li><a href="<?=$path?>hack.php#dat">Data &amp; Resources</a></li>
                <li class="right"><a href="<?=$path?>projects.php">[ Teams &amp; Projects ]</a></li>
            </ul>
        </nav>
