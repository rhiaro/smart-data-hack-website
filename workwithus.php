<? include 'top.php'; ?>
<? require_once 'workwithus_processform.php'; ?>

        <header class="light clearfix">
            <div class="color3-bg">
                <p class="wrapper unpad"><img src="img/edlogo_sm.jpg" width="70px" alt="The University of Edinburgh" /> Innovative Learning Week in Informatics <span class="right" style="margin-top:1.4em"><strong>18th - 22nd February 2013</strong></span></p>
            </div>
            <div class="color1-texture clearfix"><hgroup class="wrapper inner">
                <h1 class="w1of2 unpad">Smart Data Hack</h1>
                <div class="w1of2 align-center" style="margin-top: 3em">
                    <p><strong>Doing smart things with data to benefit local people</strong></p>
                    <p>If you've got some data you'd like to see used, a problem that to be solved or expertise to share, get in touch here.</p>
                    <p class="btn color1"><a href="#con" class="darker-border light-bg">Talk to us</a></p>
                    <p>If you're a student interested in taking part, <a href="hack.php">find out more and register a team here.</a></p>
                </div>
            </hgroup></div>
        </header>

        <nav id="sticky-nav" class="clearfix">
            <ul class="wrapper inner">
                <li><a href="#top">Home</a></li>
                <li><a href="#wor">Work with us</a></li>
                <li><a href="#sch">Schedule</a></li>
                <li><a href="#con">Get in touch</a></li>
            </ul>
        </nav>

        <div class="wrapper lighter-bg inner">
            <p>The University of Edinburgh's <em>Innovative Learning Week</em> gives students a chance to take a break from the usual timetable and do something different to enrich their academic experience.  In Informatics, we're introducing students to challenges faced by local organisations and community groups so that we can explore ways in which open data can be used to solve some of these problems.  Students will be generating ideas and hacking with a range of technologies over the course of a week to generate prototype web and mobile applications which benefit the groups involved, or the public at large.</p>
            <p>We want to work with local groups and organisations, even if you've never done anything like this before.  You don't need to know anything about open data to find some value here.  Open data can be a great tool for social empowerment... if you do want to learn more about it, a good place to start is this <a href="http://vimeo.com/21711338#" target="_blank">video about open government data</a> by the <a href="http://okfn.org/" target="_blank">Open Knowledge Foundation</a>.</p>
            <p>We're also seeking mentors and people who can run technical workshops.  We'd love to hear from professional designers and developers, and postgradaute or more experienced students.  Keep reading to find out more.</p>
            <h2 id="wor">Work with us</h2>
            <p>There are loads of ways of both supporting and benefiting from this event.  Sponsors and partners are encouraged to give a short introduction to themselves (or their data or the challenges they're providing, as appropriate) during the opening of the event on the morning of Monday the 18th.  This will take place in Inspace <a href="https://maps.google.com/maps?q=1+crichton+street+edinburgh&hl=en&ll=55.944586,-3.186786&spn=0.008868,0.026157&sll=55.94473,-3.186293&sspn=0.017832,0.052314&t=h&hnear=1+Crichton+St,+United+Kingdom&z=16">(map)</a>.</p>
            <h3>Share your problems</h3>
            <p>Whatever the size or scope of your organisation, if you're based locally to Edinburgh and face challenges that might be solveable with technology, you can talk to us about getting involved.  Maybe there's some software or an app you wish you had, data about the local area that would make a huge difference if you could access it, or a just vague feeling that a technical solution could benefit your organisation.  Understanding the problems that you face can help us to decide where to best focus our efforts, and we can work together to propose challenges for the students to work on.</p>
            <h3>Share your data</h3>
            <p>Alternatively, maybe you've been collecting data about something (in any format), and want to see it used in new or interesting ways?  Or you have an idea for an app that could use it or a visualisation of some kind.  We'd love to offer it up to the students to experiment with, and we can help you convert it into alternative formats if need be.</p>
            <h3>Teach or mentor</h3>
            <p>Students will have varying levels of technical knowledge and programming experience, so it's really important that there's plenty of opportunity for them to learn.  We're looking for people with appropriate skills to mentor students or deliver short workshops over the course of the week.  Useful topics include:</p>
            <ul>
                <li>Frontend web app stuff (HTML5, CSS3, JavaScript, mobile-first development)</li>
                <li>Web and app user interface design</li>
                <li>Native app programming (Android, iOS)</li>
                <li>Server-side stuff (PHP, Python, Ruby, and relevant frameworks)</li>
                <li>Data wrangling, visualisation and analysis</li>
                <li>Linked Data or Semantic Web technologies</li>
            </ul>
            <p>Mentoring can involve hanging out in the hacking space (for an amount of time and on days to suit you) and being available for students to ask for help with technical problems.  Distance mentors are also welcome, via IRC, instant messaging or social networks.  If you'd like to mentor in person, let us know what days you'll be available and what your areas of expertise are.  (We'll also ask you for a photo to put on the website, so that students can easily identify you).</p>
            <p>If you'd like to run a workshop, the primary slots are on Monday afternoon, and obviously earlier in the week would be better for the students in terms of progressing their projects, but let us know when would suit you.</p>
            <h3>Sponsor</h3>
            <p>Finally, we need to be able to reward the students for all of their hard work.  If you'd like to sponsor a prize or catering, we'd love to hear from you!</p>
            <? include 'schedule.php'; ?>
            <h2 id="con">Get in touch</h2>
            <? //var_dump($_POST); ?>
            <?if(isset($errors['mail'])):?>
                <p class="fail"><?=$errors['mail']?></p>
            <?endif?>
            <?if(isset($sent) && $sent):?>
                <? unset($errors); ?>
                <p class="win">Thanks for getting in touch!  One of the organising team will get back to you as soon as possible.</p>
            <?endif?>
            <iframe src="https://docs.google.com/spreadsheet/embeddedform?formkey=dGZEdDl0ZW1jWDV4SkdiLTBENlZ6cWc6MQ" width="100%" height="900" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
	</div>
        
<? include 'end.php'; ?>
