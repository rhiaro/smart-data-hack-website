<?php 
/*
UserCake Version: 2.0.1
http://usercake.com
*/
require_once("u/models/config.php");
securePage($_SERVER['PHP_SELF']);

$teams = getAllTeams();
if(empty($teams)) $teams = array();

include 'top.php';
?>

<? include 'top_hack.php'; ?>
<? include 'nav_hack.php'; ?>

<div class="wrapper lighter-bg inner clearfix">
	<h2 class="first">Teams &amp; Projects</h2>
	<p>Check out what everybody is doing...</p>
	<p><a href="teams.php">Full participant list here</a>.</p>
	<p>There's an awesome, low-tech solution for nominating someone for the Most Helpful Participant prize: email their name to Amy.Guy@ed.ac.uk!  You can only nominate one person, not yourself.  They and you must be taking part in the hack.  I will only accept your first nomination (no changing your mind). Nominations close at 2pm on Friday.</p>
	<?foreach($teams as $team):?>
		<div class="w1of1 clearfix">
			<div class="w1of2"><div class="inner">
				<h3><?=$team['name']?></h3>
				<?
				$aTeam = getTeamMembers($team['id']);
				if(!empty($aTeam)){
					foreach($aTeam as $person){
						echo "<p>";
						if($person['leader']) echo "<strong>";
						echo $person['displayname'];
						if($person['leader']) echo "</strong>";
						if(strlen($person['about']) > 0) echo " - <em>\"".$person['about']."\"</em>";
						echo "</p>";
					}
				}else{
					echo "<p><em>Nobody in this team</em></p>";
				}
				?>
			</div></div>
			<div class="w1of2"><div class="inner">
				<? $project = getProject($team['id']); ?>
				<h3><?=$project['name']?></h3>
				<p><?=$project['pitch']?></p>
				<?=(!empty($project['web'])) ? "<p>Website: <a href=\"//".$project['web']."\">".$project['web']."</a></p>" : ""?>
				<?=(!empty($project['repo'])) ? "<p>Code repo: <a href=\"//".$project['repo']."\">".$project['repo']."</a></p>" : ""?>
			</div></div>

		</div>
	<?endforeach?>

</div>
<? include 'end.php'; ?>
