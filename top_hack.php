        <header class="light clearfix">
            <div class="color3-bg">
                <p class="wrapper unpad"><img src="<?=$path?>img/edlogo_sm.jpg" width="70px" alt="The University of Edinburgh" /> Innovative Learning Week in Informatics <span class="right" style="margin-top:1.4em"><strong>18th - 22nd February 2013</strong></span></p>
            </div>    
            <div class="color1-texture clearfix"><hgroup class="wrapper inner">
                <h1 class="w1of2 unpad">Smart Data Hack</h1>
                <div class="w1of2 align-center" style="margin-top: 2em">
                    <p><strong><a href="https://twitter.com/search?q=ilwhack">#ilwhack</a></strong></p>
                    <p><strong>Doing smart things with data to benefit local people</strong></p>
                    <?if(isUserLoggedIn()):?>
                        <p>Hi <?=$loggedInUser->displayname?> (<a href="<?=$path?>u/logout.php">logout</a>)</p>
                        <p class="btn-big color1"><a href="<?=$path?>u/index.php" class="darker-border light-bg">Your team &amp; project</a></p>
                        <p class="btn-big color1"><a href="<?=$path?>u/account.php" class="darker-border light-bg">Account details</a></p>
                    <?else:?>
                        <p>Take part!  If you're a student interested in hacking, let us know here.  You can register yourself, then create or join a team at any time.</p>
                        <p><span class="btn color1"><a href="<?=$path?>u/register.php" class="darker-border light-bg">Register</a></span> <span style="margin-left: 1.4em"> or <a href="<?=$path?>u/login.php">login</a></span></p>
                        <p>If you're not a student, but have problems that need solving, data that needs using or expertise to share, <a href="workwithus.php">find out how you can get involved</a>.</p>
                    <?endif?>
                </div>
            </hgroup></div>
        </header>
