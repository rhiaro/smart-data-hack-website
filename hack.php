<? include 'top.php'; ?>

        <? include 'top_hack.php'; ?>

        <? include 'nav_hack.php'; ?>

        <div class="wrapper lighter-bg inner">
            <p>Team up with your friends and take as much or as little of Innovative Learning Week as you like to make something cool with open data.</p>
            <p><a href="http://goo.gl/2zGqe">Introductory slides</a></p>
            <p>We'll provide a great venue for hacking, plenty of free food, workshops and mentors, and prizes at the end of it all.  Use this as a chance to finally make a great idea you've had, to learn some new languages and skills, to have more great ideas, and if you like, to take up one of our challenges. </p>
            <p>Read on to find out...</p>
            <ul style="text-decoration: underline">
                <li><a href="#who">who this is for</a></li>
                <li><a href="#what">what skills or exprience you should have</a></li>
                <li><a href="#why">why you should bother</a></li>
                <li><a href="#egs">what kind of stuff you should make</a></li>
                <li><a href="#where">where it's all happening</a></li>
            </ul>
            <h3 id="who">Who?</h3><p>This event is open to all University of Edinburgh students who can join a team to do something innovative with data, creating a working prototype by the end of the week.  Useful skills and areas of interest include:</p>
            <ul>
                <li>Design</li>
                <li>Web development and programming</li>
                <li>Data analysis and problem solving</li>
            </ul>
            <p>You can register individually, and create or join teams at any time.  <strong>Don't worry</strong> if you don't have a team by the first day, we'll sort everybody out on the Monday afternoon!  You can see <a href="teams.php">a list of registered teams and individuals here</a>.</p>
            <h3 id="what">What do I need to know?</h3>
            <p>There's no specific knowledge or experience required to take part in this event.  Whether you're a designer, statistician or programmer, all you need is a willingness to get stuck in, work with other people and learn what you can.</p><p>In fact, if you've never made a web or native app before, this is a perfect opportunity to add some of these skills to your resum&#233;.</p>
            <h3 id="why">Why?</h3>
            <p>You'll come out of this event with new technical skills to show off, an appreciation of working with large datasets or unfamiliar APIs, and if everything goes to plan, a flashy app or website to add to your portfolio.  You'll also likely have just spent a week working with up to four other people... evidence of 'teamwork' for your CV!</p>
            <p>If you give it your all, there's a good chance you'll win one (or more!) of six awesome prizes.  At the very least, you'll get free food every day.</p>
            <p>You'll also get a warm, fuzzy feeling from creating something that could give real benefit local people or community groups.</p>
            <h3 id="egs">What should I make?</h3>
            <p>Look at the challenges, check out the data and think about what kinds or problems you want to address.  Then think about the skills in your team, and look at what you can learn from the workshops and mentors to help you decide if you want to make a web app, a visualisation, or a native app for some device.  Anything goes!  Some of the challenges are pretty broad, and it should be easy to make something that fits under more than one...</p>
            <p>Check out what some teams made for an <a href="http://www.guardian.co.uk/news/datablog/2011/nov/16/silicon-valley-uk-appathon-winners">Appathon last year</a> with UK Government data.</p>
            <h3 id="where">Where?</h3><p>Most things will be happening in the open area and 4.12 on AT4 (the fourth floor of Appleton Tower), with a couple of things in Inspace (across the road from Appleton Tower).  Check the schedule for more specific details.</p>
            <? include 'schedule.php'; ?>
            <h2 id="spo">Sponsors &amp; Partners</h2>
            <ul class="imgs">
                <div class="w1of1 clearfix">
                    <li class="w1of3"><a class="inner" href="http://aliss.org"><img style="width:60%; margin-top:-1em" src="img/logo_aliss.jpg" alt="ALISS" /></a></li>
                    <li class="w1of3"><a class="inner" href="http://comp-soc.com"><img src="img/logo_compsoc.png" alt="CompSoc" /></a></li>
                    <li class="w1of3"><a class="inner" href="http://edinburgh.gov.uk"><img src="img/logo_council.jpg" alt="City of Edinburgh Council" /></a></li>
                </div>
                <div class="w1of1 clearfix">
                    <li class="w1of3"><a class="inner" href="http://greenerleith.org.uk"><img src="img/logo_greenerleith.jpg" alt="Greener Leith" /></a></li>
                    <li class="w1of3"><a class="inner" href="http://insightarca.de"><img src="img/logo_ia.png" alt="Insight Arcade" style="margin-top: 1em;" /></a></li>
                    <li class="w1of3"><a class="inner" href="http://openinnovationproject.co.uk/dev/"><img src="img/logo_oi.jpg" alt="Open Innovation" style="margin-top: 1em" /></a></li>
                </div>
                <div class="w1of1 clearfix">
                    <li class="w1of3"><a class="inner" href="http://skyscanner.com"><img src="img/logo_skyscanner.jpg" alt="Skyscanner" style="padding-top: 2em"/></a></li>
                </div>
            </ul>
            <h2 id="men">Workshops &amp; Mentors</h2>
            <p>We're still putting together the schedule, but so far you can expect to have the opportunity to attend these workshops:</p>
            <div class="w1of1 clearfix">
                <div class="w1of2"><div class="inner">
                    <h3>Handling geolocated data</h3>
                    <p><strong>with Tom Armitage and Stuart MacDonald</strong>, Monday 1430-1530 in AT4.12</p>
                    <p><a href="https://docs.google.com/file/d/1riJnr7PlLJvNdDQ9s45zgHhpuOI0rzs7acfN1ZrzrKPP0sLF8GeLFU1LkJrm/edit?usp=sharing">Slides</a> and <a href="https://docs.google.com/file/d/1yzcndH4HrrbeLXEPpXH2sBM4-mwSIVxmlqptJ-ezirBKN5bicnOrqnblfuVA/edit?usp=sharing">more slides</a> and <a href="https://docs.google.com/file/d/1yA3mAg6sTl-j0oWjz7njuLvJsj4oAf4af9qfspglcdWaXQMQ3Np73bi8_aMc/edit?usp=sharing">html demo pages</a>.</p>
                </div></div>
                <div class="w1of2"><div class="inner">
                    <h3>Data visualisation with d3.js</h3>
                    <p><strong>with Philip Roberts</strong> (<a href="http://blog.latentflip.com/">latentflip.com</a>), Monday 1530-1630 in Inspace</p>
                    <p><a href="http://bit.ly/YifevM">Download slides and workshop material here</a>.</p>
                    <p><a href="d3js.org">d3.js</a> is a powerful JavaScript library that helps you build stunning and interactive visualisations, charts and infographics of your data.</p><p>This workshop will take you teach you how to get started with d3, and get you building your first interactive visualisation.</p>
                </div></div>
            </div>
            <div class="w1of1 clearfix">
                <div class="w1of2"><div class="inner">
                    <h3>Version Control for beginners</h3>
                    <p><strong>with Oli Kingshott</strong>, Monday 1530-1600 on AT4</p>
                    <p></p>
                </div></div>
                <div class="w1of2"><div class="inner">
                    <h3>Beginning Web apps with HTML5 and JavaScript</h3>
                    <p><strong>with Oli Kingshott</strong>, Monday 1600-1630 on AT4</p>
                    <p></p>
                </div></div>
            </div>
            <p>And these guys will be around at various times over the course of the week to help you out if you get stuck.  Don't be shy if you're struggling to tackle a bug, want to learn something new, or just need to talk your ideas over with someone!</p>
            <div class="w1of1 clearfix">
                <div class="w1of3">
                    <div class="inner">
                        <p><strong>Amy Guy</strong></p>
                        <p>Can help you with: HTML5, CSS3, JavaScript, Python, PHP, linked data and Semantic Web stuff.</p>
                        <p>Will be available wherever the action is, all week.</p>
                    </div>
                </div>
                <div class="w1of3">
                    <div class="inner">
                        <p><strong>Paolo Pareti</strong></p>
                        <p>Can help you with: JavaScript, Java, linked data and Semantic Web stuff, logic.</p>
                        <p>Availabile in AT4 on Wednesday and Thursday, and by email P.Pareti[at]sms.ed.ac.uk</p>
                    </div>
                </div>
                <div class="w1of3">
                    <div class="inner">
                        <p><strong>Dougal Matthews</strong></p>
                        <p>Can help you with: ALISS data and API, Python (including web frameworks), Postgres, Redis, MongoDB, and some front-end stuff.</p>
                        <p>Will be available remotely: dougal[at]dougalmatthews.com</p>
                    </div>
                </div>
            </div>
            <div class="w1of1 clearfix">
                <div class="w1of3">
                    <div class="inner">
                        <p><strong>James Baster</strong></p>
                        <p>Can help you with: HTML5, CSS3, JavaScript, PHP, Android development (Java) and Jadu API (Edinburgh Council).</p>
                        <p>Availabile on Skype on Tuesday: Contact Amy.Guy@ed.ac.uk for his Skype ID! (Other days to be confirmed)</p>
                    </div>
                </div>
                <div class="w1of3">
                    <div class="inner">
                        <p><strong>Kit Barnes</strong></p>
                        <p>Can help you with: Python, PHP, Google App Engine.</p>
                        <p>Availabile in AT4 on Tuesday and Wednesday.</p>
                    </div>
                </div>
                <div class="w1of3">
                    <div class="inner">
                        <p><strong>David Fraser</strong></p>
                        <p>Can help you with: Python (including web frameworks).</p>
                        <p>Availabile in AT4 on Tuesday and Thursday afternoons.</p>
                    </div>
                </div>
            </div>
            <div class="w1of1 clearfix">
                <div class="w1of3">
                    <div class="inner">
                        <p><strong>Oli Kingshott</strong></p>
                        <p>Can help you with: HTML5, JavaScript, Python, iOS development (Objective-C).</p>
                        <p>Availability to be confirmed.</p>
                    </div>
                </div>
                <div class="w1of3">
                    <div class="inner">
                        <p><strong>Ben Kay</strong></p>
                        <p>Can help you with: Android development (Java), JavaScript/CoffeeScript, Node.js, HTML5.</p>
                        <p>Available in AT4 on Wednesday and Thursday.</p>
                    </div>
                </div>
                <div class="w1of3">
                    <div class="inner">
                        <p><strong>Jammy Wheatley</strong></p>
                        <p>Can help you with: Windows Phone, Ruby (Rails and Sinatra), Kinect.</p>
                        <p>Availabile in AT4 on Tuesday and Thursday</p>
                    </div>
                </div>
            </div>
            <div class="w1of1 clearfix">
                <div class="w1of3">
                    <div class="inner">
                        <p><strong>James Hulme</strong></p>
                        <p>Can help you with: Python, Java, data visualisation theory and machine learning stuff.</p>
                        <p>Available in AT4 on Wednesday.</p>
                    </div>
                </div>
                <div class="w1of3">
                    <div class="inner">
                        <p><strong>Dale Myers</strong></p>
                        <p>Can help you with: Objective-C and Java.</p>
                        <p>Available in AT4 on Wednesday and Thursday.</p>
                    </div>
                </div>
                <div class="w1of3">
                    <div class="inner">
                        <p><strong>Alan Leslie</strong></p>
                        <p>Can help you with: JavaScript libraries - SIMILE timeline, Map abstraction and MIT Timemap; web page scraping using Java and XPath; GPX and KML.</p>
                        <p>Available in AT4 on Tuesday and Thursday, and at alanlesli at gmail.com all week.</p>
                    </div>
                </div>
            </div>

            <h2 id="cha">Challenges &amp; Prizes</h2>
            <p>Prizes will be awarded in the following categories:</p>
            <ul>
                <li>Best for Travel</li>
                <li>Best for Health &amp; Wellbeing</li>
                <li>Best for Communities</li>
                <li>Best UI or Visualisaiton</li>
                <li>First Year Prize for Best Data Mashup</li>
            </ul>
            <p>We'll also be taking votes over the course of the week to award someone with a <strong>Most Helpful Participant</strong> prize!
            <p>Prizes include stacks of <strong>Nexus 7</strong>s and <strong>Kindle Fire</strong>s, as well as some Google goodies.  More specific details to follow!</p>
            <h3>Need ideas?</h3>
            <p>If you're looking for real world problems to solve, or just need some inspiration, take up one of these challenges:</p>
            <h3>Making data accessible for non-technical organisations</h3>
            <p><strong>Proposed by:</strong> Greener Leith</p>
            <p><strong>Useful datasets:</strong> <a href="http://opendata.tellmescotland.gov.uk">Tell Me Scotland Open Data portal</a></p>
            <p><strong>More details</strong></p>
            <p>Level 1: Produce <a href="http://en.wikipedia.org/wiki/GeoRSS">GeoRSS</a> feeds of the Tell Me Scotland data that covers the Broughton Spurtle and Greener Leith patches, which can be used to power Greener Leith sites and apps.</p>
            <p>Level 2: Build a web interface to allow anyone to build a GeoRSS feed from the Tell Me Scotland data, including searching and selecting specific data topics and geographical locations.</p>
            <p>Level 3: Incorporate this functionality into a <a href="http://codex.wordpress.org/Plugins">Wordpress plugin</a> so that anyone with a self-hosted wordpress blog can use it.</p>
            <h2 id="dat">Data &amp; Resources</h2>
            <p>Technologies available that you might find interesting or useful include:</p>
            <ul>
                <li>A Kinect, lent to us by CompSoc.</li>
            </ul>
            <p>Here are some datasets you might find useful.</p>
            <p>A collection of datasets of interest can be found at the <strong><a href="http://data.inf.ed.ac.uk/catalog/group/ilwhack">Informatics Data Catalogue (data.inf.ed.ac.uk/catalog)</a></strong>.  If you find other useful datasets, you can even add them to the catalogue yourself for the benefit of everyone.</p>
            <p>Don't forget there are loads of big open data portals out there. Like:</p>
            <ul>
                <li><a href="https://sites.google.com/site/scotlandsdata/dataandvocabularies">List of Scotland open datasets</a></li>
                <li>data.gov.uk (UK government data, variety of formats)</li>
                <li>datahub.io (Searchable catalogue of loads of datasets)</li>
            </ul>
            <h3>ALISS</h3>
            <p><strong><a href="http://aliss.org">aliss.org</a></strong> The ALISS engine is an aggregation of Edinburgh-centric data about improving health and wellbeing and coping with long-term health conditions.  This data, which has been gathered in a number of ways, over a number of years, includes events, support groups and community venues.</p>
            <p><strong>The API</strong> is read-only, and can be queried to return JSON. <a href="http://aliss-engineclub.readthedocs.org/en/latest/engine/api.html">Documentation</a></p>
            <p><strong>Linked datasets</strong> are <a href="http://data.aliss.org/datasets">here</a> and can be queried via SPARQL or downloaded directly as RDF/XML, Turtle, N-triples or JSON.
            <h3>Skyscanner</h3>
            <p>Downloads and docs can be found on the <a href="http://data.inf.ed.ac.uk/catalog/dataset/skyscanner">Data Catalog</a>.</p>
            <p>Email Amy.Guy@ed.ac.uk for an API key for the Live API.</p>
            <p><strong>Contact</strong> <a href="http://twitter.com/aidanskyscanner">@aidanskyscanner</a>.</p>
            <h3>Edinburgh Council</h3>
            <p>Information on the <a href="http://data.inf.ed.ac.uk/catalog/dataset/ed-council">Data Catalogue</a>.</p>
            <p>Contact Amy.Guy@ed.ac.uk for an API key!</p>
            <p>Contact Sally.Kerr@edinburgh.gov.uk, <a href="http://twitter.com/WeeBletherer">@WeeBletherer</a>, for more information.</p>
            <h3>Organisation and Repository Identification (ORI) Service</h3>
            <p><strong><a href="http://ori.edina.ac.uk/">ori.edina.ac.uk</a></strong> is an edited union of several authoritative and extant data sources about repositories and organisations.</p>
            <p><strong>The User Guide</strong> is <a href="http://ori.edina.ac.uk/">here</a>.</p>
            <p><strong>The API</strong> is documented <a href="http://ori.edina.ac.uk/index.html#gtoc3">here</a> with <a href="http://ori.edina.ac.uk/index.html#gtoc3">examples</a>.</p>
            <p><strong>The SPARQL endpoint</strong> is documented <a href="http://ori.edina.ac.uk/sparql.html">here</a> with <a href="http://ori.edina.ac.uk/sparql.html#gtoc3">examples</a>.</p>
            <p><strong>The RDF ontology</strong> is documented <a href="http://ori.edina.ac.uk/ontology.html">here</a>.</p>
            <h3>Tell Me Scotland</h3>
            <p><strong><a href="http://opendata.tellmescotland.gov.uk">The Tell Me Scotland Open Data portal</a></strong> is Scotland's national public information notices (PINs) portal.  It was developed by the Scottish Government in partnership with Scotland's local authorities to provide the following national benefits:</p><ul>
            <li>An integrated national picture of statutory and public service developments</li><li>More accessible public notices with enhanced information across Scotland</li><li>Greater interactivity between service users and suppliers locally and nationally, including consumers, businesses, councils and other public service providers</li></ul>
            <p><strong>Browse</strong> the dataset with your browser at the link above.</p>
            <p><strong>Linked datasets</strong> can be accessed in RDF form, starting at http://opendata.tellmescotland.gov.uk/all</p>
            <p><strong>The SPARQL endpoint</strong> can be queried via http://opendata.tellmescotland.gov.uk/sparql</p>
            <h3>Insight Arcade</h3>
            <p><strong>API</strong> and docs at <a href="http://educationhackers.org/">educationhackers.org</a>.</p>
            <p><strong>Contact</strong> alan@insightarca.de, <a href="http://twitter.com/5l">@5l</a>.</p>
        </div>
<? include 'end.php'; ?>