<?php 
/*
UserCake Version: 2.0.1
http://usercake.com
*/
require_once("models/config.php");

securePage($_SERVER['PHP_SELF']);

//Prevent the user visiting the logged in page if he/she is already logged in
if(isUserLoggedIn()) { header("Location: account.php"); die(); }

//Forms posted
if(!empty($_POST))
{
	$errors = array();
	$email = trim($_POST["email"]);
	$username = trim($_POST["username"]);
	$displayname = trim($_POST["displayname"]);
	$matric_no = $_POST['matric_no'];
	$dietary = trim($_POST['dietary']);
	$about = trim($_POST['about']);
	$password = trim($_POST["password"]);
	$confirm_pass = trim($_POST["passwordc"]);
	$captcha = md5($_POST["captcha"]);
	
	
	if ($captcha != $_SESSION['captcha'])
	{
		$errors[] = lang("CAPTCHA_FAIL");
	}
	if(minMaxRange(2,25,$username))
	{
		$errors[] = lang("ACCOUNT_USER_CHAR_LIMIT",array(5,25));
	}
	if(!ctype_alnum($username)){
		$errors[] = lang("ACCOUNT_USER_INVALID_CHARACTERS");
	}
	if(minMaxRange(2,25,$displayname))
	{
		$errors[] = lang("ACCOUNT_DISPLAY_CHAR_LIMIT",array(2,25));
	}
	if(minMaxRange(7,7,$matric_no))
	{
		$errors[] = lang("ACCOUNT_MATRIC_CHAR_LIMIT");
	}
	if(!ctype_digit($matric_no)){
		$errors[] = lang("ACCOUNT_MATRIC_INVALID_CHARACTERS");
	}
	if(minMaxRange(0,140,$dietary))
	{
		$errors[] = lang("ACCOUNT_DIETARY_CHAR_LIMIT",array(0,140));
	}
	if(minMaxRange(0,512,$about))
	{
		$errors[] = lang("ACCOUNT_ABOUT_CHAR_LIMIT",array(0,512));
	}
	if(minMaxRange(8,50,$password) && minMaxRange(8,50,$confirm_pass))
	{
		$errors[] = lang("ACCOUNT_PASS_CHAR_LIMIT",array(8,50));
	}
	else if($password != $confirm_pass)
	{
		$errors[] = lang("ACCOUNT_PASS_MISMATCH");
	}
	if(!isValidEmail($email))
	{
		$errors[] = lang("ACCOUNT_INVALID_EMAIL");
	}
	//End data validation
	if(count($errors) == 0)
	{	
		//Construct a user object
		$user = new User($username,$displayname,$password,$email,$matric_no,$dietary,$about);
		
		//Checking this flag tells us whether there were any errors such as possible data duplication occured
		if(!$user->status)
		{
			if($user->username_taken) $errors[] = lang("ACCOUNT_USERNAME_IN_USE",array($username));
			//if($user->displayname_taken) $errors[] = lang("ACCOUNT_DISPLAYNAME_IN_USE",array($displayname));
			if($user->email_taken) 	  $errors[] = lang("ACCOUNT_EMAIL_IN_USE",array($email));		
		}
		else
		{
			//Attempt to add the user to the database, carry out finishing  tasks like emailing the user (if required)
			if(!$user->userCakeAddUser())
			{
				if($user->mail_failure) $errors[] = lang("MAIL_ERROR");
				if($user->sql_failure)  $errors[] = lang("SQL_ERROR");
			}
		}
	}
	if(count($errors) == 0) {
		$successes[] = $user->success;
	}
}

include '../top.php';
?>

<? include '../top_hack.php'; ?>
<? include '../nav_hack.php'; ?>

<div class="wrapper lighter-bg inner clearfix">
	<h2 class="first">Register</h2>
	<p class="wee">Data collected will be used to facilitate coordinating the Smart Data Hack.  Names, teams and project details may be published on this website and passed on to third parties when it is in the interest of you, or the organisation of this event (eg. in order to judge winners).  No other information will be shared with third parties, or used for communication about anything other than this event, without your express consent.</p>
	<? echo resultBlock($errors,$successes); ?>
	<form name="newUser" action="<?=$_SERVER['PHP_SELF']?>" method="post">
		<div class="w1of2"><div class="inner">
			<p>
				<label for="displayname" class="neat">Name:</label>
				<input type="text" class="neat" id="displayname" name="displayname" />
			</p>
			<p>
				<label for="matric" class="neat">Matric no: s</label>
				<input type="text" class="neat" id="matric" name="matric_no" />
			</p>
			<p>
				<label for="email" class="neat">Email:</label>
				<input type="text" class="neat" name="email" />
			</p>
			<p>
				<label for="dietary">Dietary requirements / special requests*:</label>
				<textarea class="neat" id="dietary" name="dietary"></textarea>
			</p>
			<p class="wee">* We can't guarantee any special requests, but we'll take them into consideration..</p>
			<p>
				<label for="about">Tell us about yourself... why are you taking part? What do you want to learn/build/etc? What are your skills? (This will show up on the team listings page, and might help with putting teams together at the last minute if necessary).</label>
				<textarea class="neat" id="about" name="about" ></textarea>
			</p>
		</div></div>
		<div class="w1of2"><div class="inner">
			<p>
				<label for="username" class="neat">Username:</label>
				<input type="text" class="neat" id="username" name="username" />
			</p>
			<p>
				<label for="password" class="neat">Password:</label>
				<input type="password" class="neat" name="password" />
			</p>
			<p>
				<label for="passwordc" class="neat">Confirm:</label>
				<input type="password" class="neat" id="passwordc" name="passwordc" />
			</p>
				<label for="captcha" class="neat">Enter code <img src='models/captcha.php'></label>
				<input name="captcha" class="neat" id="captcha" type="text" />
			</p>
			<p><input type="submit" value="Register"/></p>
		</div></div>
	</form>
</div>
<? include '../end.php'; ?>
