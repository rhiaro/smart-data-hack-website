<?php 
/*
UserCake Version: 2.0.1
http://usercake.com
*/
require_once("models/config.php");
securePage($_SERVER['PHP_SELF']);

//Prevent the user visiting the logged in page if he is not logged in
if(!isUserLoggedIn()) { header("Location: login.php"); die(); }

if(!empty($_POST))
{
	$errors = array();
	$successes = array();
	$displayname = trim($_POST["displayname"]);
	$password = $_POST["password"];
	$password_new = $_POST["passwordc"];
	$password_confirm = $_POST["passwordcheck"];
	
	$errors = array();
	$email = sanitize($_POST["email"]);
	$matric_no = sanitize($_POST["matric_no"]);
	$dietary = sanitize($_POST["dietary"]);
	$about = sanitize($_POST["about"]);
	
	//Perform some validation
	//Feel free to edit / change as required
	
	//Confirm the hashes match before updating a users password
	$entered_pass = generateHash($password,$loggedInUser->hash_pw);
	
	if (trim($password) == ""){
		$errors[] = lang("ACCOUNT_SPECIFY_PASSWORD");
	}
	else if($entered_pass != $loggedInUser->hash_pw)
	{
		//No match
		$errors[] = lang("ACCOUNT_PASSWORD_INVALID");
	}	
	if($email != $loggedInUser->email)
	{
		if(trim($email) == "")
		{
			$errors[] = lang("ACCOUNT_SPECIFY_EMAIL");
		}
		else if(!isValidEmail($email))
		{
			$errors[] = lang("ACCOUNT_INVALID_EMAIL");
		}
		else if(emailExists($email))
		{
			$errors[] = lang("ACCOUNT_EMAIL_IN_USE", array($email));	
		}
		
		//End data validation
		if(count($errors) == 0)
		{
			$loggedInUser->updateEmail($email);
			$successes[] = lang("ACCOUNT_EMAIL_UPDATED");
		}
	}
	if($displayname != $loggedInUser->displayname)
	{
		if(minMaxRange(2,25,$displayname))
		{
			$errors[] = lang("ACCOUNT_DISPLAY_CHAR_LIMIT",array(2,25));
		}
		
		//End data validation
		if(count($errors) == 0)
		{
			$loggedInUser->updateDisplayName($displayname);
			$successes[] = lang("ACCOUNT_DETAILS_UPDATED");
		}
	}

	if($matric_no != $loggedInUser->matric_no)
	{
		if(minMaxRange(7,7,$matric_no))
		{
			$errors[] = lang("ACCOUNT_MATRIC_CHAR_LIMIT");
		}
		if(!ctype_digit($matric_no)){
			$errors[] = lang("ACCOUNT_MATRIC_INVALID_CHARACTERS");
		}
		
		//End data validation
		if(count($errors) == 0)
		{
			$loggedInUser->updateMatricNo($matric_no);
			$successes[] = lang("ACCOUNT_DETAILS_UPDATED");
		}
	}

	if($dietary != $loggedInUser->dietary)
	{
		if(minMaxRange(0,140,$dietary))
		{
			$errors[] = lang("ACCOUNT_DIETARY_CHAR_LIMIT",array(0,140));
		}
		
		//End data validation
		if(count($errors) == 0)
		{
			$loggedInUser->updateDietary($dietary);
			$successes[] = lang("ACCOUNT_DETAILS_UPDATED");
		}
	}

	if($about != $loggedInUser->about)
	{
		if(minMaxRange(0,512,$about))
		{
			$errors[] = lang("ACCOUNT_ABOUT_CHAR_LIMIT",array(0,512));
		}
		
		//End data validation
		if(count($errors) == 0)
		{
			$loggedInUser->updateAbout($about);
			$successes[] = lang("ACCOUNT_DETAILS_UPDATED");
		}
	}
	
	if ($password_new != "" OR $password_confirm != "")
	{
		if(trim($password_new) == "")
		{
			$errors[] = lang("ACCOUNT_SPECIFY_NEW_PASSWORD");
		}
		else if(trim($password_confirm) == "")
		{
			$errors[] = lang("ACCOUNT_SPECIFY_CONFIRM_PASSWORD");
		}
		else if(minMaxRange(8,50,$password_new))
		{	
			$errors[] = lang("ACCOUNT_NEW_PASSWORD_LENGTH",array(8,50));
		}
		else if($password_new != $password_confirm)
		{
			$errors[] = lang("ACCOUNT_PASS_MISMATCH");
		}
		
		//End data validation
		if(count($errors) == 0)
		{
			//Also prevent updating if someone attempts to update with the same password
			$entered_pass_new = generateHash($password_new,$loggedInUser->hash_pw);
			
			if($entered_pass_new == $loggedInUser->hash_pw)
			{
				//Don't update, this fool is trying to update with the same password Â¬Â¬
				$errors[] = lang("ACCOUNT_PASSWORD_NOTHING_TO_UPDATE");
			}
			else
			{
				//This function will create the new hash and update the hash_pw property.
				$loggedInUser->updatePassword($password_new);
				$successes[] = lang("ACCOUNT_PASSWORD_UPDATED");
			}
		}
	}
	if(count($errors) == 0 AND count($successes) == 0){
		$errors[] = lang("NOTHING_TO_UPDATE");
	}
}

include '../top.php';
?>


<? include '../top_hack.php'; ?>
<? include '../nav_hack.php'; ?>

<div class="wrapper lighter-bg inner clearfix">
	<h3>Account details (<?=$loggedInUser->username?>)</h3>
	<p class="wee">Enter your password to make any changes</p>
	<? echo resultBlock($errors,$successes); ?>
	<form name="updateAccount" action="<?=$_SERVER['PHP_SELF']?>" method="post">
		<p>
			<label for="password" class="neat">Password:</label>
			<input type="password" id="password" class="neat" name="password" />
		</p>
		<div class="w1of2"><div class="inner">
			<p>
				<label for="displayname" class="neat">Name:</label>
				<input type="text" class="neat" name="displayname" id="displayname" value="<?=$loggedInUser->displayname?>" />
			</p>
			<p>
				<label for="email" class="neat">Email:</label>
				<input type="text" class="neat" id="email" name="email" value="<?=$loggedInUser->email?>" />
			</p>
			<p>
				<label for="matric" class="neat">Matric no: s</label>
				<input type="text" class="neat" id="matric" name="matric_no" value="<?=$loggedInUser->matric_no?>" />
			</p>
			<p>
				<label for="newpass" class="neat">New Password:</label>
				<input type="password" class="neat" name="passwordc" />
			</p>
			<p>
				<label for="passwordcheck" class="neat">Confirm Password:</label>
				<input type="password" class="neat" id="passwordcheck" name="passwordcheck" />
			</p>
		</div></div>
		<div class="w1of2"><div class="inner">
			<p>
				<label for="dietary">Dietary requirements / special requests:</label>
				<textarea class="neat" id="dietary" name="dietary"><?=$loggedInUser->dietary?></textarea>
			</p>
			<p>
				<label for="about">A little bit about you:</label>
				<textarea class="neat" id="about" name="about" ><?=$loggedInUser->about?></textarea>
			</p>
			<p>
				<input type="submit" value="Update" class="submit" />
			</p>
		</div></div>
	</form>
</div>
<? include '../end.php'; ?>
