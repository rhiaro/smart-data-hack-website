<?php 
/*
UserCake Version: 2.0.1
http://usercake.com
*/
require_once("models/config.php");
securePage($_SERVER['PHP_SELF']);
$userId = $_GET['id'];

//Check if selected user exists
if(!userIdExists($userId)){
	header("Location: admin_users.php"); die();
}

$userdetails = fetchUserDetails(NULL, NULL, $userId); //Fetch user details

//Forms posted
if(!empty($_POST))
{	
	//Delete selected account
	if(!empty($_POST['delete'])){
		$deletions = $_POST['delete'];
		if ($deletion_count = deleteUsers($deletions)) {
			$successes[] = lang("ACCOUNT_DELETIONS_SUCCESSFUL", array($deletion_count));
		}
		else {
			$errors[] = lang("SQL_ERROR");
		}
	}
	else
	{
		//Update display name
		if ($userdetails['display_name'] != $_POST['display']){
			$displayname = trim($_POST['display']);
			
			//Validate display name
			if(displayNameExists($displayname))
			{
				$errors[] = lang("ACCOUNT_DISPLAYNAME_IN_USE",array($displayname));
			}
			elseif(minMaxRange(5,25,$displayname))
			{
				$errors[] = lang("ACCOUNT_DISPLAY_CHAR_LIMIT",array(5,25));
			}
			elseif(!ctype_alnum($displayname)){
				$errors[] = lang("ACCOUNT_DISPLAY_INVALID_CHARACTERS");
			}
			else {
				if (updateDisplayName($userId, $displayname)){
					$successes[] = lang("ACCOUNT_DISPLAYNAME_UPDATED", array($displayname));
				}
				else {
					$errors[] = lang("SQL_ERROR");
				}
			}
			
		}
		else {
			$displayname = $userdetails['display_name'];
		}
		
		//Activate account
		if(isset($_POST['activate']) && $_POST['activate'] == "activate"){
			if (setUserActive($userdetails['activation_token'])){
				$successes[] = lang("ACCOUNT_MANUALLY_ACTIVATED", array($displayname));
			}
			else {
				$errors[] = lang("SQL_ERROR");
			}
		}
		
		//Update email
		if ($userdetails['email'] != $_POST['email']){
			$email = trim($_POST["email"]);
			
			//Validate email
			if(!isValidEmail($email))
			{
				$errors[] = lang("ACCOUNT_INVALID_EMAIL");
			}
			elseif(emailExists($email))
			{
				$errors[] = lang("ACCOUNT_EMAIL_IN_USE",array($email));
			}
			else {
				if (updateEmail($userId, $email)){
					$successes[] = lang("ACCOUNT_EMAIL_UPDATED");
				}
				else {
					$errors[] = lang("SQL_ERROR");
				}
			}
		}
		
		//Update title
		if ($userdetails['title'] != $_POST['title']){
			$title = trim($_POST['title']);
			
			//Validate title
			if(minMaxRange(1,50,$title))
			{
				$errors[] = lang("ACCOUNT_TITLE_CHAR_LIMIT",array(1,50));
			}
			else {
				if (updateTitle($userId, $title)){
					$successes[] = lang("ACCOUNT_TITLE_UPDATED", array ($displayname, $title));
				}
				else {
					$errors[] = lang("SQL_ERROR");
				}
			}
		}
		
		//Remove permission level
		if(!empty($_POST['removePermission'])){
			$remove = $_POST['removePermission'];
			if ($deletion_count = removePermission($remove, $userId)){
				$successes[] = lang("ACCOUNT_PERMISSION_REMOVED", array ($deletion_count));
			}
			else {
				$errors[] = lang("SQL_ERROR");
			}
		}
		
		if(!empty($_POST['addPermission'])){
			$add = $_POST['addPermission'];
			if ($addition_count = addPermission($add, $userId)){
				$successes[] = lang("ACCOUNT_PERMISSION_ADDED", array ($addition_count));
			}
			else {
				$errors[] = lang("SQL_ERROR");
			}
		}
		
		$userdetails = fetchUserDetails(NULL, NULL, $userId);
	}
}

$userPermission = fetchUserPermissions($userId);
$permissionData = fetchAllPermissions();

include '../top.php';
?>

<div class="wrapper lighter-bg inner clearfix">
	<h2>Admin User</h2>
	<? include 'user_links.php'; ?>
	<? echo resultBlock($errors,$successes); ?>
	<form name="adminUser" action="<?=$_SERVER['PHP_SELF']?>?id=<?=$userId?>" method="post">
		<div class="w1of2"><div class="inner">
			<h3>User Information</h3>
			<p><label>ID:</label> <?=$userdetails['id']?></p>
			<p><label>Username:</label> <?=$userdetails['user_name']?></p>
			<p>
				<label for="display">Display Name:</label> 
				<input type="text" id="display" name="display" value="<?=$userdetails['display_name']?>" />
			</p>
			<p>
				<label for="email">Email:</label> 
				<input type="text" id="email" name="email" value="<?=$userdetails['email']?>" />
			</p>
			<p>
				<label for="activate">Activate:</label>
				<?=($userdetails['active'] == '1') ? "active" : "<input type=\"checkbox\" name=\"activate\" id=\"activate\" value=\"activate\">"?>
			</p>
			<p>
				<label for="title">Title:</label>
				<input type="text" id="title" name="title" value="<?=$userdetails['title']?>" />
			</p>
			<p>
				<label>Sign Up:</label>
				<?=date("j M, Y", $userdetails['sign_up_stamp'])?>
			</p>
			<p>
				<label>Last Sign In:</label> 
				<?=($userdetails['last_sign_in_stamp'] == '0') ? "Never" : date("j M, Y", $userdetails['last_sign_in_stamp'])?>
			</p>
			<p>
				<label>Delete:</label> 
				<input type="checkbox" name="delete[<?=$userdetails['id']?>]" id="delete[<?=$userdetails['id']?>]" value="<?=$userdetails['id']?>" />
			</p>
			<p><input type="submit" value="Update" class="submit" /></p>
		</div></div>
		<div class="w1of2"><div class="inner">
			<h3>Permission Membership</h3>
			<p>Remove Permission:</p>
			<ul>
				<? //List of permission levels user is apart of
				foreach ($permissionData as $v1) {
					if(isset($userPermission[$v1['id']])){
						echo "<li><input type=\"checkbox\" name=\"removePermission[{$v1['id']}]\" id=\"removePermission[{$v1['id']}]\" value=\"{$v1['id']}\" /> {$v1['name']}</li>";
					}
				}
				?>
			</ul>
			<p>Add Permission:</p>
			<ul>
				<? //List of permission levels user is not apart of
				foreach ($permissionData as $v1) {
					if(!isset($userPermission[$v1['id']])){
						echo "<li><input type=\"checkbox\" name=\"addPermission[{$v1['id']}]\" id=\"addPermission[{$v1['id']}]\" value=\"{$v1['id']}\" /> {$v1['name']}</li>";
					}
				}
				?>
			</ul>
		</div></div>
	</form>
</div>
<? include '../end.php'; ?>
