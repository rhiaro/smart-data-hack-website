<?php 
/*
UserCake Version: 2.0.1
http://usercake.com
*/
require_once("models/config.php");
securePage($_SERVER['PHP_SELF']);

//Forms posted
if(!empty($_POST))
{
	//Delete permission levels
	if(!empty($_POST['delete'])){
		$deletions = $_POST['delete'];
		if ($deletion_count = deletePermission($deletions)){
		$successes[] = lang("PERMISSION_DELETIONS_SUCCESSFUL", array($deletion_count));
		}
		else {
			$errors[] = lang("SQL_ERROR");	
		}
	}
	
	//Create new permission level
	if(!empty($_POST['newPermission'])) {
		$permission = trim($_POST['newPermission']);
		
		//Validate request
		if (permissionNameExists($permission)){
			$errors[] = lang("PERMISSION_NAME_IN_USE", array($permission));
		}
		elseif (minMaxRange(1, 50, $permission)){
			$errors[] = lang("PERMISSION_CHAR_LIMIT", array(1, 50));	
		}
		else{
			if (createPermission($permission)) {
			$successes[] = lang("PERMISSION_CREATION_SUCCESSFUL", array($permission));
		}
			else {
				$errors[] = lang("SQL_ERROR");
			}
		}
	}
}

$permissionData = fetchAllPermissions(); //Retrieve list of all permission levels

include '../top.php';
?>

<div class="wrapper lighter-bg inner clearfix">
	<h2>Admin Permissions</h2>
	<? include 'user_links.php'; ?>
	<? echo resultBlock($errors,$successes); ?>
	<form name="adminPermissions" action="<?=$_SERVER['PHP_SELF']?>" method="post">
		<p>Delete permissions:</p>
		<ul>
			<?foreach($permissionData as $v1):?>
				<li>
					<input type="checkbox" name="delete[<?=$v1['id']?>]" id="delete[<?=$v1['id']?>]" value="<?=$v1['id']?>">
					<a href="admin_permission.php?id=<?=$v1['id']?>"><?=$v1['name']?></a>
				</li>
			<?endforeach?>
		</ul>
		<p>
			<label for="new">Permission Name:</label>
			<input type="text" id="new" name="newPermission" />
		</p>
		<p><input type="submit" name="Submit" value="Submit" /></p>
	</form>
</div>
<? include '../end.php'; ?>
