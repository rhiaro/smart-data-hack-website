<?php 
/*
UserCake Version: 2.0.1
http://usercake.com
*/
require_once("models/config.php");
securePage($_SERVER['PHP_SELF']);

if(isset($_POST['team_sub'])){

	$new_team = trim($_POST['nteam']);
	$join_team = $_POST['jteam'];

	if(!userInTeam()){
		if(strlen($new_team) > 0){
			if(minMaxRange(2,140,$new_team))
			{
				$errors[] = lang("ACCOUNT_TEAM_CHAR_LIMIT",array(2,140));
			}
			
			//End data validation
			if(count($errors) == 0)
			{
				$new = createTeam($new_team);
				$successes[] = lang("ACCOUNT_DETAILS_UPDATED");
			}
		}

		if($join_team != "null"){
			$join = joinTeam($join_team);
			$successes[] = lang("ACCOUNT_DETAILS_UPDATED");
		}
	}else{
		$errors[] = ("You can't join a team more than once!");
	}
}

if(isset($_POST['team_upd_sub'])){

	$id = $_POST['team_id'];
	if(isset($_POST['leave'])) $leave = true;
	else $leave = false;

	if($leave){
		leaveTeam($id, $loggedInUser->user_id);
	}else{
		if(isset($_POST['rename'])){
			$new_name = trim($_POST['rename']);
			if(minMaxRange(2,140,$new_name))
			{
				$errors[] = lang("ACCOUNT_TEAM_CHAR_LIMIT",array(2,140));
			}
			
			//End data validation
			if(count($errors) == 0)
			{
				updateTeam($id, $new_name);
				$successes[] = lang("ACCOUNT_DETAILS_UPDATED");
			}
		}
	}

}

if(isset($_POST['proj_sub'])){

	$project_name = $_POST['pname'];
	$project_pitch = $_POST['ppitch'];
	$website = $_POST['pweb'];
	$repo = $_POST['prepo'];

	if(minMaxRange(2,140,$project_name))
	{
		$errors[] = lang("ACCOUNT_PNAME_CHAR_LIMIT",array(2,140));
	}
	if(minMaxRange(0,1024,$project_pitch))
	{
		$errors[] = lang("ACCOUNT_PPITCH_CHAR_LIMIT",array(0,1024));
	}
	if(minMaxRange(0,128,$website))
	{
		$errors[] = lang("ACCOUNT_PWEB_CHAR_LIMIT",array(0,128));
	}
	if(minMaxRange(0,128,$repo))
	{
		$errors[] = lang("ACCOUNT_PREPO_CHAR_LIMIT",array(0,128));
	}
	
	//End data validation
	if(count($errors) == 0)
	{
		createProject($project_name, $project_pitch, $website, $repo);
		$successes[] = lang("ACCOUNT_PDETAILS_UPDATED");
	}

}

$teams = getAllTeams(true);
$user_team = $loggedInUser->getTeam();
if(!empty($user_team)){
	$project = getProject($user_team['team_id']);	
	$leader = getLeader($user_team['team_id']);
	$members = getTeamMembers($user_team['team_id']);
	$nextLeader = getNewLeader($user_team['team_id']);
}else{
	$project = array();	
	$leader = 0;
	$members = array();
} 

include '../top.php';
?>

<? include '../top_hack.php'; ?>
<? include '../nav_hack.php'; ?>

<div class="wrapper lighter-bg inner clearfix">
	<div class="w1of2"><div class="inner">
		<h3>Your team</h3>
		<? echo resultBlock($errors,$successes); ?>
		<?if($loggedInUser->inTeam()):?>
			<p>
				<?=($user_team['leader'] ? "You're the leader of " : "You're in ")?>
				<?=$user_team['team_name']?>
			</p>
			<p>Your members are:</p>
			<ul>
				<?foreach($members as $m):?>
					<li><?=($m['leader'] ? "<strong>" : "")?><?=$m['displayname']?> (<?=$m['email']?>)<?=($m['leader'] ? "</strong>" : "")?></li>
				<?endforeach?>
			</ul>
			<form method="post">
				<input type="hidden" name="team_id" value="<?=$user_team['team_id']?>" />
				<?if($user_team['leader']):?>
					<p>
						<label for="rename">Rename team: </label>
						<input id="rename" class="neat" name="rename" value="<?=$user_team['team_name']?>" />
					</p>
				<?endif?>
				<p><input type="checkbox" name="leave" id="leave" value="true" /> <label for="leave">Leave team?</label></p>
				<?if($user_team['leader']):?>
					<?if(!empty($nextLeader)):?>
						<p class="wee">Leadership will be passed to <?=$nextLeader['name']?>, who has been in the team longest after you.</p>
					<?else:?>
						<p class="wee">There's no-one else in the team, so it and all corresponding project information will be removed.</p>
					<?endif?>
				<?endif?>
				<p><input type="submit" name="team_upd_sub" value="Update" /></p>
			</form>
		<?else:?>
			<form method="post">
				<p>
					<label for="jteam">Join a team:</label>
					<?if(!empty($teams)):?>
						<select name="jteam" id="jteam">
							<option value="null">&nbsp;</option>
							<?foreach($teams as $team):?>
								<option value="<?=$team['id']?>"><?=$team['name']?></option>
							<?endforeach?>
						</select>
					<?else:?>
						<em>No teams available.</em>
					<?endif?>
				</p>
				<p class="wee">Teams with five members are full, and won't show up here.</p>
				<p>
					<label for="nteam">or create a new team:</label>
					<input type="text" class="neat" id="nteam" name="nteam" />
				</p>
				<p class="wee">If you need inspiration, <a href="http://www.teamnames.net/fantasy/random-team-name-generator" target="_blank">generate random team names here</a>...</p>
				<p class="wee">By creating a new team, you become the team leader and main contact point for the team.  You'll be able to rename the team.  Anyone can add themselves to your team, but you will be able to remove them if you want.</p>
				<p><input type="submit" name="team_sub" value="Save" /></p>
			</form>
		<?endif?>
	</div></div>
	<div class="w1of2"><div class="inner">
		<h3>Your team's project</h3>
		<?if($loggedInUser->inTeam()):?>
			<form method="post">
				<p>
					<label for="pname">Project name or working title</label>
					<input type="text" id="pname" class="neat" name="pname" value="<?=$project['name']?>" />
				</p>
				<p>
					<label for="ppitch">Project description</label>
					<textarea id="ppitch" class="neat" name="ppitch"><?=$project['pitch']?></textarea>
				</p>
				<p>
					<label for="pweb">Project or team website</label>
					<input type="text" id="pweb" class="neat" name="pweb" value="<?=$project['web']?>" />
				</p>
				<p>
					<label for="prepo">Code repository</label>
					<input type="text" id="prepo" class="neat" name="prepo" value="<?=$project['repo']?>" />
				</p>
				<p><input type="submit" name="proj_sub" value="Save" /></p>
			</form>
		<?else:?>
			<p class="fail">You need to join a team to have a project (even if it's just a team of you).</p>
		<?endif?>
	</div></div>
</div>
<? include '../end.php'; ?>
