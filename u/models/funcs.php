<?php
/*
UserCake Version: 2.0.1
http://usercake.com
*/

//Functions that do not interact with DB
//------------------------------------------------------------------------------

//Retrieve a list of all .php files in models/languages
function getLanguageFiles()
{
	$directory = "models/languages/";
	$languages = glob($directory . "*.php");
	//print each file name
	return $languages;
}

//Retrieve a list of all .css files in models/site-templates 
function getTemplateFiles()
{
	$directory = "models/site-templates/";
	$languages = glob($directory . "*.css");
	//print each file name
	return $languages;
}

//Retrieve a list of all .php files in root files folder
function getPageFiles()
{
	$directory = "";
	$pages = glob($directory . "*.php");
	//print each file name
	foreach ($pages as $page){
		$row[$page] = $page;
	}
	return $row;
}

//Destroys a session as part of logout
function destroySession($name)
{
	if(isset($_SESSION[$name]))
	{
		$_SESSION[$name] = NULL;
		unset($_SESSION[$name]);
	}
}

//Generate a unique code
function getUniqueCode($length = "")
{	
	$code = md5(uniqid(rand(), true));
	if ($length != "") return substr($code, 0, $length);
	else return $code;
}

//Generate an activation key
function generateActivationToken($gen = null)
{
	do
	{
		$gen = md5(uniqid(mt_rand(), false));
	}
	while(validateActivationToken($gen));
	return $gen;
}

//@ Thanks to - http://phpsec.org
function generateHash($plainText, $salt = null)
{
	if ($salt === null)
	{
		$salt = substr(md5(uniqid(rand(), true)), 0, 25);
	}
	else
	{
		$salt = substr($salt, 0, 25);
	}
	
	return $salt . sha1($salt . $plainText);
}

//Checks if an email is valid
function isValidEmail($email)
{
	return preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",trim($email));
}

//Inputs language strings from selected language.
function lang($key,$markers = NULL)
{
	global $lang;
	if($markers == NULL)
	{
		$str = $lang[$key];
	}
	else
	{
		//Replace any dyamic markers
		$str = $lang[$key];
		$iteration = 1;
		foreach($markers as $marker)
		{
			$str = str_replace("%m".$iteration."%",$marker,$str);
			$iteration++;
		}
	}
	//Ensure we have something to return
	if($str == "")
	{
		return ("No language key found");
	}
	else
	{
		return $str;
	}
}

//Checks if a string is within a min and max length
function minMaxRange($min, $max, $what)
{
	if(strlen(trim($what)) < $min)
		return true;
	else if(strlen(trim($what)) > $max)
		return true;
	else
	return false;
}

//Replaces hooks with specified text
function replaceDefaultHook($str)
{
	global $default_hooks,$default_replace;	
	return (str_replace($default_hooks,$default_replace,$str));
}

//Displays error and success messages
function resultBlock($errors,$successes){
	//Error block
	if(count($errors) > 0)
	{
		echo "<ul class='fail'>";
		foreach($errors as $error)
		{
			echo "<li>".$error."</li>";
		}
		echo "</ul>";
	}
	//Success block
	if(count($successes) > 0)
	{
		echo "<ul class='win'>";
		foreach($successes as $success)
		{
			echo "<li>".$success."</li>";
		}
		echo "</ul>";
	}
}

//Completely sanitizes text
function sanitize($str)
{
	return strip_tags(trim(($str)));
}
function sanitize2($str){
	$san = htmlspecialchars(nl2br(trim($str)));
	return str_replace("&lt;br&gt;", "<br/>", str_replace("&lt;br /&gt;", "<br />", str_replace("&lt;br/&gt;", "<br/>", $san)));
}

//Functions that interact mainly with .users table
//------------------------------------------------------------------------------

//Delete a defined array of users
function deleteUsers($users) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."users 
		WHERE id = ?");
	$stmt2 = $mysqli->prepare("DELETE FROM ".$db_table_prefix."user_permission_matches 
		WHERE user_id = ?");
	foreach($users as $id){
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$stmt2->bind_param("i", $id);
		$stmt2->execute();
		$i++;
	}
	$stmt->close();
	$stmt2->close();
	return $i;
}

//Check if a display name exists in the DB
function displayNameExists($displayname)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT active
		FROM ".$db_table_prefix."users
		WHERE
		display_name = ?
		LIMIT 1");
	$stmt->bind_param("s", $displayname);	
	$stmt->execute();
	$stmt->store_result();
	if ($stmt->num_rows > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
	$stmt->close();
}

//Check if an email exists in the DB
function emailExists($email)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT active
		FROM ".$db_table_prefix."users
		WHERE
		email = ?
		LIMIT 1");
	$stmt->bind_param("s", $email);	
	$stmt->execute();
	$stmt->store_result();
	if ($stmt->num_rows > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
	$stmt->close();
}

//Check if a user name and email belong to the same user
function emailUsernameLinked($email,$username)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT active
		FROM ".$db_table_prefix."users
		WHERE user_name = ?
		AND
		email = ?
		LIMIT 1
		");
	$stmt->bind_param("ss", $username, $email);	
	$stmt->execute();
	$stmt->store_result();
	if ($stmt->num_rows > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
	$stmt->close();
}

//Retrieve information for all users
function fetchAllUsers()
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		user_name,
		display_name,
		password,
		email,
		activation_token,
		last_activation_request,
		lost_password_request,
		active,
		title,
		sign_up_stamp,
		last_sign_in_stamp,
		matric_no,
		dietary,
		about
		FROM ".$db_table_prefix."users");
	$stmt->execute();
	$stmt->bind_result($id, $user, $display, $password, $email, $token, $activationRequest, $passwordRequest, $active, $title, $signUp, $signIn, $matric_no, $dietary, $about);
	
	while ($stmt->fetch()){
		$row[] = array('id' => $id, 'user_name' => $user, 'display_name' => sanitize2($display), 'password' => $password, 'email' => $email, 'activation_token' => $token, 'last_activation_request' => $activationRequest, 'lost_password_request' => $passwordRequest, 'active' => $active, 'title' => $title, 'sign_up_stamp' => $signUp, 'last_sign_in_stamp' => $signIn, 'matric_no' => sanitize2($matric_no), 'dietary' => sanitize2($dietary), 'about' => sanitize2($about));
	}
	$stmt->close();
	return ($row);
}

function fetchAllUserIds()
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id
		FROM ".$db_table_prefix."users");
	$stmt->execute();
	$stmt->bind_result($id);
	
	while ($stmt->fetch()){
		$row[] = $id;
	}
	$stmt->close();
	return ($row);
}

//Retrieve complete user information by username, token or ID
function fetchUserDetails($username=NULL,$token=NULL, $id=NULL)
{
	global $mysqli,$db_table_prefix; 
	if($username!=NULL) 
	{  
		$stmt = $mysqli->prepare("SELECT 
			id,
			user_name,
			display_name,
			password,
			email,
			activation_token,
			last_activation_request,
			lost_password_request,
			active,
			title,
			sign_up_stamp,
			last_sign_in_stamp,
			matric_no,
			dietary,
			about
			FROM ".$db_table_prefix."users
			WHERE
			user_name = ?
			LIMIT 1");
		$stmt->bind_param("s", $username);
	}
	elseif($id!=NULL)
	{
		$stmt = $mysqli->prepare("SELECT 
			id,
			user_name,
			display_name,
			password,
			email,
			activation_token,
			last_activation_request,
			lost_password_request,
			active,
			title,
			sign_up_stamp,
			last_sign_in_stamp,
			matric_no,
			dietary,
			about
			FROM ".$db_table_prefix."users
			WHERE
			id = ?
			LIMIT 1");
		$stmt->bind_param("i", $id);
	}
	else
	{
		$stmt = $mysqli->prepare("SELECT 
			id,
			user_name,
			display_name,
			password,
			email,
			activation_token,
			last_activation_request,
			lost_password_request,
			active,
			title,
			sign_up_stamp,
			last_sign_in_stamp,
			matric_no,
			dietary,
			about
			FROM ".$db_table_prefix."users
			WHERE
			activation_token = ?
			LIMIT 1");
		$stmt->bind_param("s", $token);
	}
	$stmt->execute();
	$stmt->bind_result($id, $user, $display, $password, $email, $token, $activationRequest, $passwordRequest, $active, $title, $signUp, $signIn, $matric_no, $dietary, $about);
	while ($stmt->fetch()){
		$row = array('id' => $id, 'user_name' => $user, 'display_name' => sanitize2($display), 'password' => $password, 'email' => $email, 'activation_token' => $token, 'last_activation_request' => $activationRequest, 'lost_password_request' => $passwordRequest, 'active' => $active, 'title' => $title, 'sign_up_stamp' => $signUp, 'last_sign_in_stamp' => $signIn, 'matric_no' => sanitize2($matric_no), 'dietary' => sanitize2($dietary), 'about' => sanitize2($about));
	}
	$stmt->close();
	return ($row);
}

//Toggle if lost password request flag on or off
function flagLostPasswordRequest($username,$value)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET lost_password_request = ?
		WHERE
		user_name = ?
		LIMIT 1
		");
	$stmt->bind_param("ss", $value, $username);
	return $stmt->execute();
	$stmt->close();
}

//Check if a user is logged in
function isUserLoggedIn()
{
	global $loggedInUser,$mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT 
		id,
		password
		FROM ".$db_table_prefix."users
		WHERE
		id = ?
		AND 
		password = ? 
		AND
		active = 1
		LIMIT 1");
	$stmt->bind_param("is", $loggedInUser->user_id, $loggedInUser->hash_pw);	
	$stmt->execute();
	$stmt->store_result();
	if($loggedInUser == NULL)
	{
		return false;
	}
	else
	{
		if ($stmt->num_rows > 0)
		{
			return true;
		}
		else
		{
			destroySession("userCakeUser");
			return false;	
		}
	}
	$stmt->close();
}

//Change a user from inactive to active
function setUserActive($token)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET active = 1
		WHERE
		activation_token = ?
		LIMIT 1");
	$stmt->bind_param("s", $token);
	return $stmt->execute();
	$stmt->close();	
}

//Change a user's display name
function updateDisplayName($id, $display)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET display_name = ?
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("si", $display, $id);
	return $stmt->execute();
	$stmt->close();	
}

//Update a user's email
function updateEmail($id, $email)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET 
		email = ?
		WHERE
		id = ?");
	$stmt->bind_param("si", $email, $id);
	return $stmt->execute();
	$stmt->close();	
}

//Input new activation token, and update the time of the most recent activation request
function updateLastActivationRequest($new_activation_token,$username,$email)
{
	global $mysqli,$db_table_prefix; 	
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET activation_token = ?,
		last_activation_request = ?
		WHERE email = ?
		AND
		user_name = ?");
	$stmt->bind_param("ssss", $new_activation_token, time(), $email, $username);
	return $stmt->execute();
	$stmt->close();	
}

//Generate a random password, and new token
function updatePasswordFromToken($pass,$token)
{
	global $mysqli,$db_table_prefix;
	$new_activation_token = generateActivationToken();
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET password = ?,
		activation_token = ?
		WHERE
		activation_token = ?");
	$stmt->bind_param("sss", $pass, $new_activation_token, $token);
	return $stmt->execute();
	$stmt->close();	
}

//Update a user's title
function updateTitle($id, $title)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET 
		title = ?
		WHERE
		id = ?");
	$stmt->bind_param("si", $title, $id);
	return $stmt->execute();
	$stmt->close();	
}

//Check if a user ID exists in the DB
function userIdExists($id)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT active
		FROM ".$db_table_prefix."users
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("i", $id);	
	$stmt->execute();
	$stmt->store_result();
	if ($stmt->num_rows > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
	$stmt->close();
}

//Checks if a username exists in the DB
function usernameExists($username)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT active
		FROM ".$db_table_prefix."users
		WHERE
		user_name = ?
		LIMIT 1");
	$stmt->bind_param("s", $username);	
	$stmt->execute();
	$stmt->store_result();
	if ($stmt->num_rows > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
	$stmt->close();
}

//Check if activation token exists in DB
function validateActivationToken($token,$lostpass=NULL)
{
	global $mysqli,$db_table_prefix;
	if($lostpass == NULL) 
	{	
		$stmt = $mysqli->prepare("SELECT active
			FROM ".$db_table_prefix."users
			WHERE active = 0
			AND
			activation_token = ?
			LIMIT 1");
	}
	else 
	{
		$stmt = $mysqli->prepare("SELECT active
			FROM ".$db_table_prefix."users
			WHERE active = 1
			AND
			activation_token = ?
			AND
			lost_password_request = 1 
			LIMIT 1");
	}
	$stmt->bind_param("s", $token);
	$stmt->execute();
	$stmt->store_result();
	if ($stmt->num_rows > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
	$stmt->close();
}


//Functions that interact mainly with teams and projects
//------------------------------------------------------------------------------

function createTeam($team)
{
	global $mysqli, $db_table_prefix, $loggedInUser;
	$u = $loggedInUser->user_id;
	$now = (int)time();
	$leader = 1;
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."team_projects (
		team_name, 
		created_by
		)
		VALUES(
		?,
		?
		)");
	$stmt->bind_param("si", $team, $u);
	if($stmt->execute()){
		$team_id = $stmt->insert_id;
	}
	$stmt2 = $mysqli->prepare("INSERT INTO ".$db_table_prefix."users_teams (
		user_id, team_id, leader, date_modified)
		VALUES(?,?,?,?)
		");
	$stmt2->bind_param("iiii", $u, $team_id, $leader, $now);
	$out = $stmt2->execute();

	$stmt->close();
	$stmt2->close();

	return $out;
}

function updateTeam($id, $name)
{
	global $mysqli, $db_table_prefix, $loggedInUser;
	$u = $loggedInUser->user_id;
	$now = time();

	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."team_projects
		SET team_name = ?, last_modified = ?, last_modified_by = ?
		WHERE
		team_id = ?
		LIMIT 1");
	$stmt->bind_param("siii", $name, $now, $u, $id);
	return $stmt->execute();
	$stmt->close();	
}

function joinTeam($team)
{
	global $mysqli, $db_table_prefix, $loggedInUser;
	$u = $loggedInUser->user_id;
	$now = time();

	//if(!userInTeam()){
		$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."users_teams (
		user_id, team_id, leader, date_modified, is_del)
		VALUES(?,?,0,?,0)
		");
		$stmt->bind_param("sii", $u, $team, $now);
		return $stmt->execute();
		$stmt->close();	
	//}else{
	//		return false;
	//}
}

function getTeamMembers($team_id)
{
	global $mysqli, $db_table_prefix;
	$db = $db_table_prefix;
	$row = array();

	$stmt = $mysqli->prepare("SELECT 
		".$db."users_teams.leader,
		".$db."team_projects.team_name,
		".$db."users.display_name,
		".$db."users.email,
		".$db."users.matric_no,
		".$db."users.dietary,
		".$db."users.about,
		".$db."users.user_name
		FROM ".$db."users_teams
		INNER JOIN ".$db."team_projects ON ".$db."users_teams.team_id = ".$db."team_projects.team_id
		INNER JOIN ".$db."users ON ".$db."users_teams.user_id = ".$db."users.id
		WHERE ".$db."users_teams.team_id = ? AND ".$db."team_projects.is_del = 0 AND ".$db."users_teams.is_del = 0
		ORDER BY ".$db."users_teams.leader DESC, ".$db."users_teams.date_modified ASC
		");
	$stmt->bind_param("i", $team_id);
	$stmt->execute();
	$stmt->bind_result($leader, $team_name, $display_name, $email, $matric_no, $dietary, $about, $username);
	while ($stmt->fetch()){
		$row[] = array('team_name' => sanitize2($team_name), 'leader' => $leader, 'displayname' => sanitize2($display_name), 'email' => $email, 'matric_no' => sanitize2($matric_no), 'dietary' => sanitize2($dietary), 'about' => sanitize2($about), 'username' => $username);
	}
	$stmt->close();
	return ($row);
}

function countTeamMembers($team_id)
{
	global $mysqli, $db_table_prefix;
	$db = $db_table_prefix;
	$row = array();

	$stmt = $mysqli->prepare("SELECT COUNT(user_id)
		FROM ".$db."users_teams
		WHERE team_id = ?
		");
	$stmt->bind_param("i", $team_id);
	$stmt->execute();
	$stmt->bind_result($count);
	while($stmt->fetch()){
		$c = $count;
	}
	$stmt->close();
	return ($c);
}

function getAllTeams($spaces=false)
{
	global $mysqli, $db_table_prefix;
	$db = $db_table_prefix;

	$row = array();

	$stmt = $mysqli->prepare("SELECT
		team_id, team_name
		FROM
		".$db."team_projects
		WHERE is_del != 1
		");
	$stmt->execute();
	$stmt->bind_result($id, $name);
	while($stmt->fetch()){
		$row[] = array('id'=>$id, 'name'=>sanitize2($name));
	}
	$stmt->close();
	if($spaces && !empty($row)){
		foreach($row as $team){
			$c = countTeamMembers($team['id']);
			if($c < 5) $out[] = $team;
		}
	}else{
		$out = $row;
	}
	return $out;
}

function createProject($name, $descr, $web, $repo)
{
	global $mysqli, $db_table_prefix, $loggedInUser;
	$u = $loggedInUser->user_id;
	$ut = $loggedInUser->getTeam();
	$t = $ut['team_id'];
	$now = (int)time();
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."team_projects 
		SET project_name = ?, 
			project_pitch = ?, 
			website = ?, 
			repo = ?, 
			last_modified = ?, 
			last_modified_by = ?
		WHERE
			team_id = ?
		");
	$stmt->bind_param("ssssiii", $name, $descr, $web, $repo, $now, $u, $t);
	$out = $stmt->execute();

	$stmt->close();

	return $out;
}

function getProject($team_id)
{
	global $mysqli, $db_table_prefix;
	$db = $db_table_prefix;
	$row = array();
	$stmt = $mysqli->prepare("SELECT
		project_name, project_pitch, website, repo, last_modified, last_modified_by
		FROM
		".$db."team_projects
		WHERE team_id = ?
		LIMIT 1
		");
	$stmt->bind_param("i", $team_id);
	$stmt->execute();
	$stmt->bind_result($name, $pitch, $website, $repo, $last_mod, $mod_by);
	while($stmt->fetch()){
		$row = array('name' => sanitize2($name), 'pitch' => sanitize2($pitch), 'web' => sanitize2($website), 'repo' => sanitize2($repo), 'last_mod' => $last_mod, 'mod_by' => $mod_by);
	}
	$stmt->close();
	return $row;
}

function getLeader($team_id)
{
	global $mysqli, $db_table_prefix;
	$db = $db_table_prefix;
	$row = array();
	$stmt = $mysqli->prepare("SELECT
		".$db."users_teams.user_id, ".$db."users.display_name, ".$db."users.email
		FROM ".$db."users_teams
		INNER JOIN ".$db."users 
		ON ".$db."users.id = ".$db."users_teams.user_id
		WHERE team_id = ? AND leader = 1 AND is_del = 0
		LIMIT 1
		");
	$stmt->bind_param("i", $team_id);
	$stmt->execute();
	$stmt->bind_result($id, $name, $email);
	while($stmt->fetch()){
		$row = array('id' => $id, 'name' => sanitize2($name), 'email' => $email);
	}
	$stmt->close();
	return $row;
}

function userInTeam()
{
	global $mysqli, $db_table_prefix, $loggedInUser;
	$db = $db_table_prefix;
	$user_id = $loggedInUser->user_id;
	$stmt = $mysqli->prepare("SELECT
		COUNT(id)
		FROM ".$db."users_teams
		WHERE user_id = ? AND is_del = 0
		");
	$stmt->bind_param("i", $user_id);
	$stmt->execute();
	$stmt->bind_result($c);
	$stmt->fetch();
	if($c > 0){
		return true;
	}else{
		return false;
	}
}

function getNewLeader($team_id)
{
	global $mysqli, $db_table_prefix;
	$db = $db_table_prefix;
	$row = array();
	$stmt = $mysqli->prepare("SELECT
		".$db."users_teams.user_id, ".$db."users.display_name
		FROM ".$db."users_teams
		INNER JOIN ".$db."users 
		ON ".$db."users.id = ".$db."users_teams.user_id
		WHERE team_id = ? AND leader = 0 AND is_del = 0
		ORDER BY ".$db."users_teams.date_modified ASC
		LIMIT 1
		");
	$stmt->bind_param("i", $team_id);
	$stmt->execute();
	$stmt->bind_result($id, $name);
	while($stmt->fetch()){
		$row = array('id' => $id, 'name' => sanitize2($name));
	}
	$stmt->close();
	return $row;
}

function replaceLeader($team_id)
{
	global $mysqli,$db_table_prefix;
	$db = $db_table_prefix;

	$old = getLeader($team_id);
	$old = $old['id'];
	$new = getNewLeader($team_id);
	$new = $new['id'];
	$stmt = $mysqli->prepare("UPDATE ".$db."users_teams
		SET leader = 0
		WHERE team_id = ? AND user_id = ? AND leader = 1
		LIMIT 1
		");
	$stmt->bind_param("ii", $team_id, $old);
	$stmt->execute();
	$stmt2 = $mysqli->prepare("UPDATE ".$db."users_teams
		SET leader = 1
		WHERE team_id = ? AND user_id = ? AND leader = 0
		LIMIT 1
		");
	$stmt2->bind_param("ii", $team_id, $new);
	$stmt2->execute();
	$stmt->close();
	$stmt2->close();
}

function leaveTeam($team_id, $user_id)
{
	global $mysqli,$db_table_prefix;
	$db = $db_table_prefix;
	$l= getLeader($team_id);
	if($l['id'] == $user_id){ // leader is leaving
		$new = getNewLeader($team_id);
		if(!empty($new)){
			replaceLeader($team_id);
		}else{ //no-one else in team, mark as deleted
			$stmt = $mysqli->prepare("UPDATE ".$db."team_projects
				SET is_del = 1, last_modified = '".time()."', last_modified_by = ?
				WHERE team_id = ?
				");
			$stmt->bind_param("ii", $user_id, $team_id);
			$stmt->execute();
			$stmt->close();
		}
	} 
	// now delete user_team row
	$stmt2 = $mysqli->prepare("UPDATE ".$db."users_teams
		SET is_del = 1, date_modified = '".time()."'
		WHERE team_id = ? AND user_id = ?
		LIMIT 1
		");
	$stmt2->bind_param("ii", $team_id, $user_id);
	$stmt2->execute();
	$stmt2->close();

}

function getUserTeam($user_id)
{
	global $mysqli,$db_table_prefix;
	$db = $db_table_prefix;

	$stmt = $mysqli->prepare("SELECT ".$db."users_teams.id, ".$db."users_teams.team_id, ".$db."users_teams.leader, ".$db."team_projects.team_name
		FROM ".$db."users_teams
		INNER JOIN ".$db."team_projects
		ON ".$db."users_teams.team_id = ".$db."team_projects.team_id
		WHERE user_id = ? AND is_del = 0");
	$stmt->bind_param("i", $user_id);
	$stmt->execute();
	$stmt->bind_result($id, $team_id, $leader, $team_name);
	$row = array();
	while ($stmt->fetch()){
		$row = array('id' => $id, 'team_id' => $team_id, 'leader' => $leader, 'team_name' => sanitize2($team_name));
	}
	$stmt->close();
	return ($row);
}

function getNotInTeam()
{
	$user_ids = fetchAllUserIds();
	$out = array();
	foreach($user_ids as $id){
		$team = getUserTeam($id);
		if(empty($team)) $out[] = fetchUserDetails(null,null,$id);
	}
	return $out;
}

//Functions that interact mainly with .permissions table
//------------------------------------------------------------------------------

//Create a permission level in DB
function createPermission($permission) {
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."permissions (
		name
		)
		VALUES (
		?
		)");
	$stmt->bind_param("s", $permission);
	return $stmt->execute();
	$stmt->close();
}

//Delete a permission level from the DB
function deletePermission($permission) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."permissions 
		WHERE id = ?");
	$stmt2 = $mysqli->prepare("DELETE FROM ".$db_table_prefix."user_permission_matches 
		WHERE permission_id = ?");
	$stmt3 = $mysqli->prepare("DELETE FROM ".$db_table_prefix."permission_page_matches 
		WHERE permission_id = ?");
	foreach($permission as $id){
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$stmt2->bind_param("i", $id);
		$stmt2->execute();
		$stmt3->bind_param("i", $id);
		$stmt3->execute();
		$i++;
	}
	$stmt->close();
	$stmt2->close();
	$stmt3->close();
	return $i;
}

//Retrieve information for all permission levels
function fetchAllPermissions()
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		name
		FROM ".$db_table_prefix."permissions");
	$stmt->execute();
	$stmt->bind_result($id, $name);
	while ($stmt->fetch()){
		$row[] = array('id' => $id, 'name' => $name);
	}
	$stmt->close();
	return ($row);
}

//Retrieve information for a single permission level
function fetchPermissionDetails($id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		name
		FROM ".$db_table_prefix."permissions
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("i", $id);
	$stmt->execute();
	$stmt->bind_result($id, $name);
	while ($stmt->fetch()){
		$row = array('id' => $id, 'name' => $name);
	}
	$stmt->close();
	return ($row);
}

//Check if a permission level ID exists in the DB
function permissionIdExists($id)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT id
		FROM ".$db_table_prefix."permissions
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("i", $id);	
	$stmt->execute();
	$stmt->store_result();
	
	if ($stmt->num_rows > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
	$stmt->close();
}

//Check if a permission level name exists in the DB
function permissionNameExists($permission)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT id
		FROM ".$db_table_prefix."permissions
		WHERE
		name = ?
		LIMIT 1");
	$stmt->bind_param("s", $permission);	
	$stmt->execute();
	$stmt->store_result();
	if ($stmt->num_rows > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
	$stmt->close();
}

//Change a permission level's name
function updatePermissionName($id, $name)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."permissions
		SET name = ?
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("si", $name, $id);
	return $stmt->execute();
	$stmt->close();	
}

//Functions that interact mainly with .user_permission_matches table
//------------------------------------------------------------------------------

//Match permission level(s) with user(s)
function addPermission($permission, $user) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."user_permission_matches (
		permission_id,
		user_id
		)
		VALUES (
		?,
		?
		)");
	if (is_array($permission)){
		foreach($permission as $id){
			$stmt->bind_param("ii", $id, $user);
			$stmt->execute();
			$i++;
		}
	}
	elseif (is_array($user)){
		foreach($user as $id){
			$stmt->bind_param("ii", $permission, $id);
			$stmt->execute();
			$i++;
		}
	}
	else {
		$stmt->bind_param("ii", $permission, $user);
		$stmt->execute();
		$i++;
	}
	$stmt->close();
	return $i;
}

//Retrieve information for all user/permission level matches
function fetchAllMatches()
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		user_id,
		permission_id
		FROM ".$db_table_prefix."user_permission_matches");
	$stmt->execute();
	$stmt->bind_result($id, $user, $permission);
	while ($stmt->fetch()){
		$row[] = array('id' => $id, 'user_id' => $user, 'permission_id' => $permission);
	}
	$stmt->close();
	return ($row);	
}

//Retrieve list of permission levels a user has
function fetchUserPermissions($user_id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT
		id,
		permission_id
		FROM ".$db_table_prefix."user_permission_matches
		WHERE user_id = ?
		");
	$stmt->bind_param("i", $user_id);	
	$stmt->execute();
	$stmt->bind_result($id, $permission);
	while ($stmt->fetch()){
		$row[$permission] = array('id' => $id, 'permission_id' => $permission);
	}
	$stmt->close();
	if (isset($row)){
		return ($row);
	}
}

//Retrieve list of users who have a permission level
function fetchPermissionUsers($permission_id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT id, user_id
		FROM ".$db_table_prefix."user_permission_matches
		WHERE permission_id = ?
		");
	$stmt->bind_param("i", $permission_id);	
	$stmt->execute();
	$stmt->bind_result($id, $user);
	while ($stmt->fetch()){
		$row[$user] = array('id' => $id, 'user_id' => $user);
	}
	$stmt->close();
	if (isset($row)){
		return ($row);
	}
}

//Unmatch permission level(s) from user(s)
function removePermission($permission, $user) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."user_permission_matches 
		WHERE permission_id = ?
		AND user_id =?");
	if (is_array($permission)){
		foreach($permission as $id){
			$stmt->bind_param("ii", $id, $user);
			$stmt->execute();
			$i++;
		}
	}
	elseif (is_array($user)){
		foreach($user as $id){
			$stmt->bind_param("ii", $permission, $id);
			$stmt->execute();
			$i++;
		}
	}
	else {
		$stmt->bind_param("ii", $permission, $user);
		$stmt->execute();
		$i++;
	}
	$stmt->close();
	return $i;
}

//Functions that interact mainly with .configuration table
//------------------------------------------------------------------------------

//Update configuration table
function updateConfig($id, $value)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."configuration
		SET 
		value = ?
		WHERE
		id = ?");
	foreach ($id as $cfg){
		$stmt->bind_param("si", $value[$cfg], $cfg);
		return $stmt->execute();
	}
	$stmt->close();	
}

//Functions that interact mainly with .pages table
//------------------------------------------------------------------------------

//Add a page to the DB
function createPages($pages) {
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."pages (
		page
		)
		VALUES (
		?
		)");
	foreach($pages as $page){
		$stmt->bind_param("s", $page);
		$stmt->execute();
	}
	$stmt->close();
}

//Delete a page from the DB
function deletePages($pages) {
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."pages 
		WHERE id = ?");
	$stmt2 = $mysqli->prepare("DELETE FROM ".$db_table_prefix."permission_page_matches 
		WHERE page_id = ?");
	foreach($pages as $id){
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$stmt2->bind_param("i", $id);
		$stmt2->execute();
	}
	$stmt->close();
	$stmt2->close();
}

//Fetch information on all pages
function fetchAllPages()
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		page,
		private
		FROM ".$db_table_prefix."pages");
	$stmt->execute();
	$stmt->bind_result($id, $page, $private);
	while ($stmt->fetch()){
		$row[$page] = array('id' => $id, 'page' => $page, 'private' => $private);
	}
	$stmt->close();
	if (isset($row)){
		return ($row);
	}
}

//Fetch information for a specific page
function fetchPageDetails($id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		page,
		private
		FROM ".$db_table_prefix."pages
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("i", $id);
	$stmt->execute();
	$stmt->bind_result($id, $page, $private);
	while ($stmt->fetch()){
		$row = array('id' => $id, 'page' => $page, 'private' => $private);
	}
	$stmt->close();
	return ($row);
}

//Check if a page ID exists
function pageIdExists($id)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT private
		FROM ".$db_table_prefix."pages
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("i", $id);	
	$stmt->execute();
	$stmt->store_result();	
	if ($stmt->num_rows > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
	$stmt->close();
}

//Toggle private/public setting of a page
function updatePrivate($id, $private)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."pages
		SET 
		private = ?
		WHERE
		id = ?");
	$stmt->bind_param("ii", $private, $id);
	return $stmt->execute();
	$stmt->close();	
}

//Functions that interact mainly with .permission_page_matches table
//------------------------------------------------------------------------------

//Match permission level(s) with page(s)
function addPage($page, $permission) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."permission_page_matches (
		permission_id,
		page_id
		)
		VALUES (
		?,
		?
		)");
	if (is_array($permission)){
		foreach($permission as $id){
			$stmt->bind_param("ii", $id, $page);
			$stmt->execute();
			$i++;
		}
	}
	elseif (is_array($page)){
		foreach($page as $id){
			$stmt->bind_param("ii", $permission, $id);
			$stmt->execute();
			$i++;
		}
	}
	else {
		$stmt->bind_param("ii", $permission, $page);
		$stmt->execute();
		$i++;
	}
		$stmt->close();
	return $i;
}

//Retrieve list of permission levels that can access a page
function fetchPagePermissions($page_id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT
		id,
		permission_id
		FROM ".$db_table_prefix."permission_page_matches
		WHERE page_id = ?
		");
	$stmt->bind_param("i", $page_id);	
	$stmt->execute();
	$stmt->bind_result($id, $permission);
	while ($stmt->fetch()){
		$row[$permission] = array('id' => $id, 'permission_id' => $permission);
	}
	$stmt->close();
	if (isset($row)){
		return ($row);
	}
}

//Retrieve list of pages that a permission level can access
function fetchPermissionPages($permission_id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT
		id,
		page_id
		FROM ".$db_table_prefix."permission_page_matches
		WHERE permission_id = ?
		");
	$stmt->bind_param("i", $permission_id);	
	$stmt->execute();
	$stmt->bind_result($id, $page);
	while ($stmt->fetch()){
		$row[$page] = array('id' => $id, 'permission_id' => $page);
	}
	$stmt->close();
	if (isset($row)){
		return ($row);
	}
}

//Unmatched permission and page
function removePage($page, $permission) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."permission_page_matches 
		WHERE page_id = ?
		AND permission_id =?");
	if (is_array($page)){
		foreach($page as $id){
			$stmt->bind_param("ii", $id, $permission);
			$stmt->execute();
			$i++;
		}
	}
	elseif (is_array($permission)){
		foreach($permission as $id){
			$stmt->bind_param("ii", $page, $id);
			$stmt->execute();
			$i++;
		}
	}
	else {
		$stmt->bind_param("ii", $permission, $user);
		$stmt->execute();
		$i++;
	}
	$stmt->close();
	return $i;
}

//Check if a user has access to a page
function securePage($uri){
	//Separate document name from uri
	$tokens = explode('/', $uri);
	$page = $tokens[sizeof($tokens)-1];
	global $mysqli,$db_table_prefix,$loggedInUser;
	//retrieve page details
	$stmt = $mysqli->prepare("SELECT 
		id,
		page,
		private
		FROM ".$db_table_prefix."pages
		WHERE
		page = ?
		LIMIT 1");
	$stmt->bind_param("s", $page);
	$stmt->execute();
	$stmt->bind_result($id, $page, $private);
	while ($stmt->fetch()){
		$pageDetails = array('id' => $id, 'page' => $page, 'private' => $private);
	}
	$stmt->close();
	//If page does not exist in DB, allow access
	if (empty($pageDetails)){
		return true;
	}
	//If page is public, allow access
	elseif ($pageDetails['private'] == 0) {
		return true;	
	}
	//If user is not logged in, deny access
	elseif(!isUserLoggedIn()) 
	{
		header("Location: login.php");
		return false;
	}
	else {
		//Retrieve list of permission levels with access to page
		$stmt = $mysqli->prepare("SELECT
			permission_id
			FROM ".$db_table_prefix."permission_page_matches
			WHERE page_id = ?
			");
		$stmt->bind_param("i", $pageDetails['id']);	
		$stmt->execute();
		$stmt->bind_result($permission);
		while ($stmt->fetch()){
			$pagePermissions[] = $permission;
		}
		$stmt->close();
		//Check if user's permission levels allow access to page
		if ($loggedInUser->checkPermission($pagePermissions)){ 
			return true;
		}
		else {
			header("Location: account.php");
			return false;	
		}
	}
}

?>
