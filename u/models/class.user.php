<?php
/*
UserCake Version: 2.0.1
http://usercake.com
*/

class loggedInUser {
	public $email = NULL;
	public $hash_pw = NULL;
	public $user_id = NULL;

	public function getTeam()
	{
		global $mysqli,$db_table_prefix;
		$db = $db_table_prefix;

		$stmt = $mysqli->prepare("SELECT ".$db."users_teams.id, ".$db."users_teams.team_id, ".$db."users_teams.leader, ".$db."team_projects.team_name
			FROM ".$db."users_teams
			INNER JOIN ".$db."team_projects
			ON ".$db."users_teams.team_id = ".$db."team_projects.team_id
			WHERE user_id = ? AND reg_users_teams.is_del = 0");
		$stmt->bind_param("i", $this->user_id);
		$stmt->execute();
		$stmt->bind_result($id, $team_id, $leader, $team_name);
		$row = array();
		while ($stmt->fetch()){
			$row = array('id' => $id, 'team_id' => $team_id, 'leader' => $leader, 'team_name' => $team_name);
		}
		$stmt->close();
		return ($row);
	}

	public function inTeam()
	{
		$team = $this->getTeam();
		if(empty($team)) return false;
		else return true;
	}
	
	//Simple function to update the last sign in of a user
	public function updateLastSignIn()
	{
		global $mysqli,$db_table_prefix;
		$time = time();
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
			SET
			last_sign_in_stamp = ?
			WHERE
			id = ?");
		$stmt->bind_param("ii", $time, $this->user_id);
		$stmt->execute();
		$stmt->close();	
	}
	
	//Return the timestamp when the user registered
	public function signupTimeStamp()
	{
		global $mysqli,$db_table_prefix;
		
		$stmt = $mysqli->prepare("SELECT sign_up_stamp
			FROM ".$db_table_prefix."users
			WHERE id = ?");
		$stmt->bind_param("i", $this->user_id);
		$stmt->execute();
		$stmt->bind_result($timestamp);
		$stmt->fetch();
		$stmt->close();
		return ($timestamp);
	}
	
	//Update a users password
	public function updatePassword($pass)
	{
		global $mysqli,$db_table_prefix;
		$secure_pass = generateHash($pass);
		$this->hash_pw = $secure_pass;
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
			SET
			password = ? 
			WHERE
			id = ?");
		$stmt->bind_param("si", $secure_pass, $this->user_id);
		$stmt->execute();
		$stmt->close();	
	}
	
	//Update a users email
	public function updateEmail($email)
	{
		global $mysqli,$db_table_prefix;
		$this->email = $email;
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
			SET 
			email = ?
			WHERE
			id = ?");
		$stmt->bind_param("si", $email, $this->user_id);
		$stmt->execute();
		$stmt->close();	
	}

	//Update a users matric no
	public function updateMatricNo($matric_no)
	{
		global $mysqli,$db_table_prefix;
		$this->matric_no = $matric_no;
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
			SET 
			matric_no = ?
			WHERE
			id = ?");
		$stmt->bind_param("si", $matric_no, $this->user_id);
		$stmt->execute();
		$stmt->close();	
	}

	//Update a users display name
	public function updateDisplayName($displayname)
	{
		global $mysqli,$db_table_prefix;
		$this->displayname = $displayname;
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
			SET 
			display_name = ?
			WHERE
			id = ?");
		$stmt->bind_param("si", $displayname, $this->user_id);
		$stmt->execute();
		$stmt->close();	
	}

	//Update a users dietary preferences
	public function updateDietary($dietary)
	{
		global $mysqli,$db_table_prefix;
		$this->dietary = $dietary;
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
			SET 
			dietary = ?
			WHERE
			id = ?");
		$stmt->bind_param("si", $dietary, $this->user_id);
		$stmt->execute();
		$stmt->close();	
	}

	//Update a users about
	public function updateAbout($about)
	{
		global $mysqli,$db_table_prefix;
		$this->about = $about;
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
			SET 
			about = ?
			WHERE
			id = ?");
		$stmt->bind_param("si", $about, $this->user_id);
		$stmt->execute();
		$stmt->close();	
	}
	
	//Is a user has a permission
	public function checkPermission($permission)
	{
		global $mysqli,$db_table_prefix;
		
		$stmt = $mysqli->prepare("SELECT id 
			FROM ".$db_table_prefix."user_permission_matches
			WHERE user_id = ?
			AND permission_id = ?
			LIMIT 1
			");
		$access = 0;
		foreach($permission as $check){
			if ($access == 0){
				$stmt->bind_param("ii", $this->user_id, $check);
				$stmt->execute();
				$stmt->store_result();
				if ($stmt->num_rows > 0){
					$access = 1;
				}
			}
		}
		if ($access == 1)
		{
			return true;
		}
		else
		{
			return false;	
		}
		$stmt->close();
	}
	
	//Logout
	public function userLogOut()
	{
		destroySession("userCakeUser");
	}	
}

?>