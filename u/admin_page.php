<?php 
/*
UserCake Version: 2.0.1
http://usercake.com
*/
require_once("models/config.php");
securePage($_SERVER['PHP_SELF']);
$pageId = $_GET['id'];

//Check if selected pages exist
if(!pageIdExists($pageId)){
	header("Location: admin_pages.php"); die();	
}

$pageDetails = fetchPageDetails($pageId); //Fetch information specific to page

//Forms posted
if(!empty($_POST)){
	$update = 0;
	
	if(!empty($_POST['private'])){ $private = $_POST['private']; }
	
	//Toggle private page setting
	if (isset($private) AND $private == 'Yes'){
		if ($pageDetails['private'] == 0){
			if (updatePrivate($pageId, 1)){
				$successes[] = lang("PAGE_PRIVATE_TOGGLED", array("private"));
			}
			else {
				$errors[] = lang("SQL_ERROR");
			}
		}
	}
	elseif ($pageDetails['private'] == 1){
		if (updatePrivate($pageId, 0)){
			$successes[] = lang("PAGE_PRIVATE_TOGGLED", array("public"));
		}
		else {
			$errors[] = lang("SQL_ERROR");	
		}
	}
	
	//Remove permission level(s) access to page
	if(!empty($_POST['removePermission'])){
		$remove = $_POST['removePermission'];
		if ($deletion_count = removePage($pageId, $remove)){
			$successes[] = lang("PAGE_ACCESS_REMOVED", array($deletion_count));
		}
		else {
			$errors[] = lang("SQL_ERROR");	
		}
		
	}
	
	//Add permission level(s) access to page
	if(!empty($_POST['addPermission'])){
		$add = $_POST['addPermission'];
		if ($addition_count = addPage($pageId, $add)){
			$successes[] = lang("PAGE_ACCESS_ADDED", array($addition_count));
		}
		else {
			$errors[] = lang("SQL_ERROR");	
		}
	}
	
	$pageDetails = fetchPageDetails($pageId);
}

$pagePermissions = fetchPagePermissions($pageId);
$permissionData = fetchAllPermissions();

include '../top.php';
?>

<div class="wrapper lighter-bg inner clearfix">
	<h2>Admin Page</h2>
	<? include 'user_links.php'; ?>
	<? echo resultBlock($errors,$successes); ?>
	<form name="adminPage" action="<?=$_SERVER['PHP_SELF']?>?id=<?=$pageId?>" method="post">
		<input type="hidden" name="process" value="1">
		<div class="w1of2"><div class="inner">
			<h3>Page Information</h3>
			<p>
				<label>ID:</label>
				<?=$pageDetails['id']?>
			</p>
			<p>
				<label>Name:</label>
				<?=$pageDetails['page']?>
			</p>
			<p>
				<label>Private:</label>";
				<? //Display private checkbox
				if ($pageDetails['private'] == 1){
					echo "<input type=\"checkbox\" name=\"private\" id=\"private\" value=\"Yes\" checked>";
				}
				else {
					echo "<input type=\"checkbox\" name=\"private\" id=\"private\" value=\"Yes\">";	
				}
				?>
			</p>
		</div></div>
		<div class="w1of2"><div class="inner">
			<h3>Page Access</h3>
			<p>Remove Access:</p>
			<ul>
				<?
				//Display list of permission levels with access
				foreach ($permissionData as $v1) {
					if(isset($pagePermissions[$v1['id']])){
						echo "<li><input type=\"checkbox\" name=\"removePermission[{$v1['id']}]\" id=\"removePermission[{$v1['id']}]\" value=\"{$v1['id']}\" /> {$v1['name']}</li>";
					}
				}
			?>
			</ul>
			<p>Add Access:</p>
			<ul>
				<?
				//Display list of permission levels without access
				foreach ($permissionData as $v1) {
					if(!isset($pagePermissions[$v1['id']])){
						echo "<li><input type=\"checkbox\" name=\"addPermission[{$v1['id']}]\" id=\"addPermission[{$v1['id']}]\" value=\"{$v1['id']}\" /> {$v1['name']}</li>";
					}
				}
				?>
			</ul>

			<p>
				<input type="submit" value="Update" />
			</p>
		</div></div>
	</form>
</div>
<? include '../end.php'; ?>
