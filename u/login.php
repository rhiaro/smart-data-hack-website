<?php 
/*
UserCake Version: 2.0.1
http://usercake.com
*/
require_once("models/config.php");
securePage($_SERVER['PHP_SELF']);

//Prevent the user visiting the logged in page if he/she is already logged in
if(isUserLoggedIn()) { header("Location: index.php"); die(); }

//Forms posted
if(!empty($_POST))
{
	$errors = array();
	$username = sanitize(trim($_POST["username"]));
	$password = trim($_POST["password"]);
	
	//Perform some validation
	//Feel free to edit / change as required
	if($username == "")
	{
		$errors[] = lang("ACCOUNT_SPECIFY_USERNAME");
	}
	if($password == "")
	{
		$errors[] = lang("ACCOUNT_SPECIFY_PASSWORD");
	}

	if(count($errors) == 0)
	{
		//A security note here, never tell the user which credential was incorrect
		if(!usernameExists($username))
		{
			$errors[] = lang("ACCOUNT_USER_OR_PASS_INVALID");
		}
		else
		{
			$userdetails = fetchUserDetails($username);
			//See if the user's account is activated
			if($userdetails["active"]==0)
			{
				$errors[] = lang("ACCOUNT_INACTIVE");
			}
			else
			{
				//Hash the password and use the salt from the database to compare the password.
				$entered_pass = generateHash($password,$userdetails["password"]);
				
				if($entered_pass != $userdetails["password"])
				{
					//Again, we know the password is at fault here, but lets not give away the combination incase of someone bruteforcing
					$errors[] = lang("ACCOUNT_USER_OR_PASS_INVALID");
				}
				else
				{
					//Passwords match! we're good to go'
					
					//Construct a new logged in user object
					//Transfer some db data to the session object
					$loggedInUser = new loggedInUser();
					$loggedInUser->email = $userdetails["email"];
					$loggedInUser->user_id = $userdetails["id"];
					$loggedInUser->hash_pw = $userdetails["password"];
					$loggedInUser->title = $userdetails["title"];
					$loggedInUser->displayname = $userdetails["display_name"];
					$loggedInUser->username = $userdetails["user_name"];
					$loggedInUser->matric_no = $userdetails["matric_no"];
					$loggedInUser->dietary = $userdetails["dietary"];
					$loggedInUser->about = $userdetails["about"];
					
					//Update last sign in
					$loggedInUser->updateLastSignIn();
					$_SESSION["userCakeUser"] = $loggedInUser;
					
					//Redirect to user account page
					header("Location: index.php");
					die();
				}
			}
		}
	}
}

include '../top.php';
?>

<? include '../top_hack.php'; ?>
<? include '../nav_hack.php'; ?>

<div class="wrapper lighter-bg inner clearfix">
	<? echo resultBlock($errors,$successes); ?>
	<div class="w1of2"><div class="inner">
		<h3>Login</h3>
		<form name="login" action="<?=$_SERVER['PHP_SELF']?>" method="post">
			<p>
				<label for="username" class="neat">Username:</label>
				<input type="text" id="username" class="neat" name="username" />
			</p>
			<p>
				<label for="password" class="neat"> Password:</label>
				<input type="password" class="neat" name="password" />
			</p>
			<p>
				<input type="submit" value="Login" class="submit" />
				<span class="wee">(<a href='forgot-password.php'>Forgotten your password?</a>)</span>
			</p>
		</form>
	</div></div>
</div>
<? include '../end.php'; ?>
