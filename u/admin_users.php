<?php 
/*
UserCake Version: 2.0.1
http://usercake.com
*/
require_once("models/config.php");
securePage($_SERVER['PHP_SELF']);

//Forms posted
if(!empty($_POST))
{
	$deletions = $_POST['delete'];
	if ($deletion_count = deleteUsers($deletions)){
		$successes[] = lang("ACCOUNT_DELETIONS_SUCCESSFUL", array($deletion_count));
	}
	else {
		$errors[] = lang("SQL_ERROR");
	}
}

$userData = fetchAllUsers(); //Fetch information for all users

include '../top.php';
?>

<div class="wrapper lighter-bg inner clearfix">
	<h2>Admin Users</h2>
	<? include 'user_links.php'; ?>
	<? echo resultBlock($errors,$successes); $c = 0; ?>
	<hr/>
	<textarea style="width:90%; height:200px"><?foreach ($userData as $v1):?><?=$v1['email']?>, <? $c++; ?><?endforeach?></textarea>
	<hr/>
	<p><strong><?=$c?> users registered</strong></p>
	<form name="adminUsers" action="<?=$_SERVER['PHP_SELF']?>" method="post">
		<table width="100%">
			<tr><th width="10px">Delete</th><th>Name</th><th>Matric</th><th>Email</th><th>Dietary</th><th>Sign up</th><th>Last Sign In</th></tr>
			<?foreach ($userData as $v1):?>
			<tr>
				<td><input type="checkbox" name="delete[<?=$v1['id']?>]" id="delete[<?=$v1['id']?>]" value="<?=$v1['id']?>"></td>
				<td><?=$v1['display_name']?> (<a href="admin_user.php?id=<?=$v1['id']?>"><?=$v1['user_name']?></a>)</td>
				<td><?=$v1['matric_no']?></td>
				<td><?=$v1['email']?></td>
				<td><?=$v1['dietary']?></td>
				<!--<td><?=$v1['title']?></td>-->
				<td><?=($v1['sign_up_stamp'] == '0') ? "Never" : date("j M, Y", $v1['sign_up_stamp'])?></td>
				<td><?=($v1['last_sign_in_stamp'] == '0') ? "Never" : date("j M, Y", $v1['last_sign_in_stamp'])?></td>
			</tr>
			<?endforeach?>
		</table>
		<p><input type="submit" name="Submit" value="Delete" /></p>
	</form>
</div>
<? include '../end.php'; ?>
