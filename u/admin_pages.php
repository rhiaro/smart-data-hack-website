<?php 
/*
UserCake Version: 2.0.1
http://usercake.com
*/
require_once("models/config.php");
securePage($_SERVER['PHP_SELF']);

$pages = getPageFiles(); //Retrieve list of pages in root usercake folder
$dbpages = fetchAllPages(); //Retrieve list of pages in pages table
$creations = array();
$deletions = array();

//Check if any pages exist which are not in DB
foreach ($pages as $page){
	if(!isset($dbpages[$page])){
		$creations[] = $page;	
	}
}

//Enter new pages in DB if found
if (count($creations) > 0) {
	createPages($creations)	;
}

if (count($dbpages) > 0){
	//Check if DB contains pages that don't exist
	foreach ($dbpages as $page){
		if(!isset($pages[$page['page']])){
			$deletions[] = $page['id'];	
		}
	}
}

//Delete pages from DB if not found
if (count($deletions) > 0) {
	deletePages($deletions);
}

//Update DB pages
$dbpages = fetchAllPages();

include '../top.php';
?>

<div class="wrapper lighter-bg inner clearfix">
	<h2>Admin Pages</h2>
	<? include 'user_links.php'; ?>
	<table>
		<tr><th>Id</th><th>Page</th><th>Access</th></tr>
		<?foreach ($dbpages as $page):?>
			<tr>
				<td><?=$page['id']?></td>
				<td><a href ="admin_page.php?id=<?=$page['id']?>"><?=$page['page']?></a></td>
				<td><?=$page['private']==0 ? "Public" : "Private"?></td>
			</tr>
		<?endforeach?>
	</table>
</div>
<? include '../end.php'; ?>
