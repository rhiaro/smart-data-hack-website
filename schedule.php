            <h2 id="sch">Schedule</h2>
            <p>The hack is taking place during Innovative Learning Week; the 18th to 22nd of February, 2013.</p>
            <p>More details to be added.  This schedule is provisional, and subject to change!</p>
            <div class="w1of1 clearfix">
                <div class="w1of2"><div class="inner">
                    <h3>Monday 18th</h3>
                    <p><strong>1000</strong> Introduction in Inspace, with drinks and snacks</p>
                    <p><strong>1030</strong> Skyscanner introduce their data and  challenges.</p>
                    <p><strong>1100</strong> ALISS introduce their data and challenges.</p>
                    <p><strong>1130</strong> City of Edinburgh Council introduce their data and challenges.</p>
                    <p><strong>1200</strong> Other data holders and challenge properosers introduce themselves.</p>
                    <p><strong>1300</strong> Lunch in Inspace</p>
                    <p><strong>1330</strong> Team formation and networking</p>
                    <p><strong>1400</strong> Hacking begins</p>
                    <p><strong>1430 - 1530</strong> Workshop 1: Handling geolocated data (AT4)</p>
                    <p><strong>1530 - 1630</strong> Workshop 2: Visualisation with d3.js (Inspace, max. 30 people)</p>
                    <p><strong>1530 - 1600</strong> Workshop 3: Version Control for beginners (AT4)</p>
                    <p><strong>1600 - 1630</strong> Workshop 4: Beginning HTML5 Web apps (AT4)</p>
                    <p><strong>1700</strong> Mobile Apps Usability talk by Skyscanner (Inspace) </p>
                    <p><strong>1800</strong> Pizza and networking</p>
                </div></div>
                <div class="w1of2"><div class="inner">
                    <h3>Tuesday 19th</h3>
                    <p><strong>1000 - 1700</strong> Hacking and snacks on AT4. <a href="hack.php#men">Mentors available</a>.</p>
                <!--</div></div>
                <div class="w1of2"><div class="inner">-->
                    <h3>Wednesday 20th</h3>
                    <p><strong>1000 - 1700</strong> Hacking and snacks on AT4. <a href="hack.php#men">Mentors available</a>.</p>
                    <p><strong>1100</strong> Lightning presentations of your progress so far.</p>
                    <p><strong>1300</strong> Lunch</p>
                <!--</div></div>
                <div class="w1of2"><div class="inner">-->
                    <h3>Thursday 21st</h3>
                    <p><strong>1000 - 1700</strong> Hacking and snacks on AT4. <a href="hack.php#men">Mentors available</a>.</p>
                <!--</div></div>
                <div class="w1of2"><div class="inner">-->
                    <h3>Friday 22nd</h3>
                    <p><strong>1000 - 1400</strong> Hacking and snacks on AT4. <a href="hack.php#men">Mentors available</a>.</p>
                    <p><strong>1400</strong> Hacking ends. Lunch.</p>
                    <p><strong>1500</strong> Demonstrations, judging and prizes.</p>
                    <p><strong>1730</strong> Fin.</p>
                </div></div>
            </div>