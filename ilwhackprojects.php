<?
header('Content-Type: text/xml');
echo '<?xml version="1.0" encoding="ISO-8859-1"?>';
?>
<rss version="2.0">  
	<channel>  
		<title>ILW Smart Data Hack</title>  
		<description>The University of Edinburgh Innovative Learning Week Smart Data Hack. Information about teams and projects.</description>  
		<link>http://data.inf.ed.ac.uk/ilwhack/projects.php</link> 
		<?
		require_once("u/models/config.php");
		require_once('u/models/funcs.php');
		$teams = getAllTeams();
		if(empty($teams)) $teams = array();
		?>
		<?foreach($teams as $team):?>
			<item>
				<team>
					<name><?=$team['name']?></name>
					<?
					$aTeam = getTeamMembers($team['id']);
					if(!empty($aTeam)){
						foreach($aTeam as $person){
							if($person['leader']) echo "<leader>"; else echo "<member>";
							echo "<name>".$person['displayname']."</name>";
							if(strlen($person['about']) > 0) echo "<bio>{$person['about']}</bio>";
							if($person['leader']) echo "</leader>"; else echo "</member>";
						}
					}
					$project = getProject($team['id']);
					?>
					<project>
						<name><?=$project['name']?></name>
						<description><?=$project['pitch']?></description>
						<?=(!empty($project['web'])) ? "<url>Website: ".$project['web']."</url>" : ""?>
						<?=(!empty($project['repo'])) ? "<repo>Code repo: ".$project['repo']."</repo>" : ""?>
						<lastModified><?=$project['last_mod']?></lastModified>
					</project>
				</team>
			</item>
		<?endforeach?>
	</channel>
</rss>